
# Battleships

![Downloads](https://img.shields.io/github/downloads/claudiu-sava/BattleShip/total) ![Contributors](https://img.shields.io/github/contributors/claudiu-sava/BattleShip?color=dark-green) ![Issues](https://img.shields.io/github/issues/claudiu-sava/BattleShip)


## About The Project

<img alt="battleship" src="./img/battleship.png"/>

Welcome aboard the thrilling world of Battleships, where strategy meets excitement on the high seas!<br/>
Challenge yourself to outsmart your opponent and sink their ships before they can sink yours. With every move, the tension mounts as you plan your next attack and defend your fleet.<br/>
But why go it alone when you can bring your friends along for the ride? Engage in epic multiplayer battles and see who has what it takes to rule the waves!<br/>
With eye-popping visuals that bring the action to life, Battleships is a feast for the senses. From the sleek designs of your ships to the explosive effects of each cannon blast, it's an immersive experience like no other.<br/>
And the best part? No need to mess around with downloads – just fire up your web browser and get ready to set sail. Whether you're on your laptop, tablet, or phone, the fun is always just a click away.<br/>
So what are you waiting for? Grab your crew, hoist the anchor, and get ready for an adventure you won't soon forget. It's time to show the world what you're made of in Battleships!<br/>

This project began during my time in the Digital Talents Program. The program not only taught me how to code but also showed me how to maintain code, work collaboratively, and conduct proper testing. The combination of knowledge gained from internships and insights from the school has resulted in the creation of this small game.

## Built With

This game is written in Java, using Spring-Boot for backend and Thymeleaf as Rendering Engine.

* [Java](https://www.java.com/)
* [Spring-Boot](https://spring.io/projects/spring-boot/)
* [Thymeleaf](https://www.thymeleaf.org/)
* [jQuerry](https://jquery.com/)
* [Bootstrap](https://getbootstrap.com/)

## Features

<img alt="battleship_play" src="./img/battleship_play.png"/>

### Game Modes

- Single player: Play against an inexperienced AI (easy)
- Single player: Play against an average experienced AI (normal)
- Single Player: Play against an expert player AI (hard)
- Single Player: Play against an hacker AI (hacker)
- Multiplayer: Play against a friend online (or local using LAN)

### Game Settings

- Game board resizing: Play using your desired board game (12x12 cells up to 15x15 cells)
- Difficulty: Play against the computer using different difficulty levels
- Smart Setting Remembering System: Battleships will remember your settings and next time you play it will pre-configure itself accordingly

### Ship Placement

- Place different sized ships on the game board: Possibility to choose between 2-5 cell ships
- Delete the incorrect placed ships: Possibility to delete wrong placed ships
- Rotate the ships: Possibility to change the ship orientation (horizontal / vertical)
- Illegal moves detection system: Battleship won't allow the placement of an incorrect ships
- Live feedback: Battleship will calculate if a ship can fit on the board or if it touches another ship, creating a red shadow to inform the user

### Personalization

- Login: Receive personalised messages and save your game scores on the Scoreboard along your username
- Guest: Allow a guest to play and save his scores on the Scoreboard using a generic identifier

### Scores

- Scoreboard based on a game
- Global scoreboard

## Dependencies

* JAVA 21

* org.springframework.boot

    * spring-boot-starter-data-jpa

    * spring-boot-starter-security

    * spring-boot-starter-thymeleaf

    * spring-boot-starter-web

    * spring-boot-starter-test

* org.springframework.security

    * spring-security-test

* org.thymeleaf.extras

    * thymeleaf-extras-springsecurity6

* org.mariadb.jdbc

    * mariadb-java-client

* org.projectlombok

    * lombok


## Installation (for developers)

**In order to start the project an ACTIVE internet connection is required.**

### Windows

1. Please ensure that JDK 21 (Java Development Kit) is installed, if not, install it from [here](https://www.oracle.com/ch-de/java/technologies/downloads/#jdk21-windows).

2. [Add the JDK to path](https://www.geeksforgeeks.org/how-to-set-java-path-in-windows-and-linux/)

* Add a new System Variable `JAVA_HOME` with the value `C:\Program Files\Java\jdk21`

* Under System Variables > Path, insert `%JAVA_HOME%\bin`

3. Clone the repository on your local machine

```bash
git clone https://github.com/claudiu-sava/BattleShip.git
```

4. Go to the Battleship folder

5. Start the project using

```bash
mvnw.cmd spring-boot:run
```

6. On your browser open `http://localhost:3333`

### Linux / Unix / Mac

1. Please ensure that JDK 21 is installed. *Use your system instructions to install it*

2. Add the JDK to path

```bash
# JDK 21 should be found here /usr/lib/jvm/<jdk 21>
# Replace <jdk 21> with the actual folder name

sudo nano /etc/profile


# Add at the end of the file

export JAVA_HOME="<jdk path>"
export PATH=$JAVA_HOME/bin:$PATH


# Save and exit nano
# Apply changes using

source /etc/profile
```

3. Clone the repository on your local machine

```bash
git clone https://github.com/claudiu-sava/BattleShip.git
```

4. Go to the Battleship folder

5. Start the project using

```bash
mvn spring-boot:run
```

6. On your browser open `http://localhost:3333`


### IntelliJ IDEA

You can start the project in IntelliJ without having to manually configure Java. Please refer to IntelliJ's instructions on how to install the SDK 21 and how to install the dependencies using Maven.

### Browser

We recommend you to use Firefox while playing Battleships.

**CAUTION: While playing multiplayer: DO NOT USE THE SAME BROWSER IF YOU WANT TO PLAY WITH YOURSELF! Please use a different browser or a different device!**

## Developer Mode

<img alt="battleship_dev" src="./img/battleship_dev.png"/>

**CAUTION: Enabling the Developer Mode will turn off the cell icons and other design elements!**</br>

Developer mode allows you to see the enemy's ship placement for debugging purposes. Here is how to enable it:

### Single Player

1. Go to src/main/resources/templates/play.html

2. At line 87, inside the button tag add `th:text="x"`. The result should look like this: `<button th:text="x" ...>`

3. Save the file and restart the app

### Multiplayer

1. Go to src/main/resources/templates/multiplayerPlay.html

2. At line 56, inside the button tag add `th:text="x"`. The result should look like this: `<button th:text="x" ...>`

3. Save the file and restart the app


## Roadmap

See the [open issues](https://github.com/claudiu-sava/BattleShip/issues) for a list of proposed features (and known issues).

### Psst, bugs

Battleships is far from perfect. If you encounter any bugs or problems, don't hesitate to create an issue.

## Contributing

Contributions are what make the open source community such an amazing place to be learn, inspire, and create. Any contributions you make are **greatly appreciated**.
* If you have suggestions for adding or removing projects, feel free to [open an issue](https://github.com/claudiu-sava/BattleShip/issues/new) to discuss it, or directly create a pull request after you edit the *README.md* file with necessary changes (like adding yourself as a contributor).
* Please make sure you check your spelling and grammar.
* Create individual PR for each suggestion.

### Creating A Pull Request

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request

## Authors

* **Claudiu Sava** - *Student / Java Backend dev* - [Claudiu Sava](https://github.com/claudiu-sava)
* **Jonathan Eickhoff** - *Student / Java dev* - [Jonathan Eickhoff](https://github.com/4E6F)