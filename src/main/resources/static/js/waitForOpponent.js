var multiplayerGameId = document.cookie.split("multiplayerGameId=")[1].split(";")[0];
var contextPath = document.cookie.split("contextPath=")[1].split(";")[0];

if(contextPath === "/"){
    contextPath = "";
}


$(window).on("load", () => {

    this.setTimeout(() => {

        checkIfEnemyIsReady();


    }, 2500);

});

function checkIfEnemyIsReady() {

    this.setTimeout(() => {

        fetch(contextPath + '/multiplayer/isEnemyReady?gameId=' + multiplayerGameId, {
            method: 'POST',
        })
            .then(response => response.text())
            .then(data => {

                if(data === "no"){
                    checkIfEnemyIsReady();
                } else {
                    window.location.href = contextPath + '/multiplayer/play?gameId=' + multiplayerGameId;
                }

            })
            .catch(error => {
                console.error('Error:', error);
            });

    }, 1000);
}