var contextPath = document.cookie.split("contextPath=")[1].split(";")[0];

if(contextPath === "/"){
    contextPath = "";
}


/**
 * Sends a request to the server to place a ship on the game board.
 *
 * @param {Array} cellList - The list of cells representing the ship's position.
 * @return {undefined} This function does not return a value.
 */
function placeShip(cellList){

    var gameId = document.cookie.split("gameId=")[1].split(";")[0];

    fetch(contextPath + '/game/placeShip' + "?gameId=" + gameId, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(cellList)
    })
        .then(response => response.text())
        .then(data => {
            console.log(data);
            jsonResponse = JSON.parse(data);

            if (jsonResponse.status === 1){
                cellList.forEach(cell => {
                    var shipY = cell.split("_")[1];
                    var shipX = cell.split("_")[2];
                    $('#fieldUser_' + shipY + "_" + shipX).text("x");
                });
            } else {
                cellList.forEach(cell => {
                    var shipY = cell.split("_")[1];
                    var shipX = cell.split("_")[2];
                    $('#fieldUser_' + shipY + "_" + shipX).text("");
                });
            }
        })
        .catch(error => {
            console.error('Error:', error);
        });
}

/**
 * Shoots at a specified location on the game board and updates the display accordingly.
 *
 * @param {string} id - The ID of the location to shoot at in the format "shipY_shipX".
 * @return {void}
 */
function shoot(id){
    var shipY = id.split("_")[1];
    var shipX = id.split("_")[2];
    var gameId = document.cookie.split("gameId=")[1].split(";")[0];

    var hitsPlayerCounter = $('#hitsPlayerCounter');
    var missesPlayerCounter = $('#missesPlayerCounter');
    var shootsPlayerCounter = $('#shootsPlayerCounter');
    var scoreCounter = $('#scoreCounter');
    var ratioPlayerCounter = $('#ratioPlayerCounter');

    fetch(contextPath + '/game/shoot' + "?shipY=" + shipY + "&shipX=" + shipX + "&gameId=" + gameId, {
        method: 'POST',
    })
        .then(response => response.text())
        .then(data => {
            console.log(data);
            var jsonResponse = JSON.parse(data);
            if(jsonResponse.status === 1){
                $('#' + id).removeClass("btn-outline-dark")
                    .addClass("btn-outline-danger")
                    .prop("disabled", true)
                    .addClass("shoot")
                    .find('img')
                    .attr('src', contextPath + "/drawable/ship_explosion.gif")
                    .removeAttr("hidden")
                    .addClass("cell");

                scoreCounter.text(parseInt(scoreCounter.text()) + 10);
                hitsPlayerCounter.text(parseInt(hitsPlayerCounter.text()) + 1);

                getRatio(ratioPlayerCounter, scoreCounter.text());

            } else {
                $('#' + id).prop("disabled", true)
                    .addClass("shoot")
                    .find('img')
                    .attr('src', contextPath + "/drawable/explosion.gif")
                    .removeAttr("hidden")
                    .addClass("cell");

                missesPlayerCounter.text(parseInt(missesPlayerCounter.text()) + 1);

                shootBack(gameId);
            }

            shootsPlayerCounter.text(parseInt(shootsPlayerCounter.text()) + 1);


        })
        .catch(error => {
            console.error('Error:', error);
        });
}

/**
 * Retrieves the ratio from the server and updates the ratio counter.
 *
 * @param {number} ratioCounter - The counter for the ratio.
 * @param {number} score - The score of the game.
 * @return {undefined} This function does not return a value.
 */
function getRatio(ratioCounter, score){
    var gameId = document.cookie.split("gameId=")[1].split(";")[0];

    fetch(contextPath + '/game/getRatio' + "?gameId=" + gameId, {
        method: 'GET',
    })
        .then(response => response.text())
        .then(data => {
            console.log(data);
            var jsonResponse = JSON.parse(data);

            ratioCounter.text((parseFloat(ratioCounter.text()) - parseFloat(jsonResponse.ratio)).toFixed(2));

            if(parseFloat(ratioCounter.text()) < parseFloat(jsonResponse.ratio)){
                window.location.href = contextPath + '/game/gameOver?winner=user&score=' + score;
            }
        })
        .catch(error => {
            console.error('Error:', error);
        });
}

/**
 * Retrieves the ratio of enemies from the server and updates the ratio counter.
 *
 * @param {number} ratioCounter - The current ratio counter.
 * @param {number} score - The player's current score.
 * @return {void} This function does not return anything.
 */
function getRatioEnemy(ratioCounter, score){
    var gameId = document.cookie.split("gameId=")[1].split(";")[0];

    fetch(contextPath + '/game/getRatioEnemy' + "?gameId=" + gameId, {
        method: 'GET',
    })
        .then(response => response.text())
        .then(data => {
            console.log(data);
            var jsonResponse = JSON.parse(data);
            ratioCounter.text((parseFloat(ratioCounter.text()) - parseFloat(jsonResponse.ratio)).toFixed(2));

            if(parseFloat(ratioCounter.text()) < parseFloat(jsonResponse.ratio)){
                window.location.href = contextPath + '/game/gameOver?winner=computer&score=' + score;
            }
        })
        .catch(error => {
            console.error('Error:', error);
        });
}

function shootBack(gameId){

    var hitsEnemyCounter = $('#hitsEnemyCounter');
    var missesEnemyCounter = $('#missesEnemyCounter');
    var shootsEnemyCounter = $('#shootsEnemyCounter');
    var scoreCounter = $('#scoreCounter');
    var enemyRatioCounter = $('#ratioEnemyCounter');

    $("button.cell").attr("disabled", true);

    fetch(contextPath + '/game/shootBack' + "?gameId=" + gameId, {
        method: 'POST',
    })
        .then(response => response.text())
        .then(data => {
            console.log(data);
            var jsonResponse = JSON.parse(data);
            if(jsonResponse.status === 1){
                var img = $('<img>').attr('src', contextPath + '/drawable/ship_explosion.gif').attr('alt', 'Image').addClass("cell");
                $('#fieldUser_' + jsonResponse.shipY + "_" + jsonResponse.shipX)
                    .removeClass("btn-outline-dark")
                    .addClass("btn-outline-danger")
                    .text("")
                    .append(img);

                scoreCounter.text(parseInt(scoreCounter.text()) - 10);
                hitsEnemyCounter.text(parseInt(hitsEnemyCounter.text()) + 1);
                getRatioEnemy(enemyRatioCounter, scoreCounter.text());

                shootBack(gameId);
            } else {
                $('#fieldUser_' + jsonResponse.shipY + "_" + jsonResponse.shipX)
                    .find('img')
                    .attr('src', contextPath + "/drawable/explosion.gif")
                    .removeAttr("hidden")
                    .addClass("cell");

                $("button.cell").attr("disabled", false);
                $("button.cell.shoot").prop("disabled", true);
                missesEnemyCounter.text(parseInt(missesEnemyCounter.text()) + 1);
            }
            shootsEnemyCounter.text(parseInt(shootsEnemyCounter.text()) + 1);

        })
        .catch(error => {
            console.error('Error:', error);
        });

}

/**
 * Generates the possible ship placement horizontally.
 *
 * @param {string} id - The ID of the cell where the ship placement is being checked.
 * @param {number} shipLength - The length of the ship being placed.
 * @return {Array<string> | null} - An array of cell IDs if the ship placement is possible, null otherwise.
 */
function showPossibleShipPlacementHorizontal(id, shipLength){

    const posY = parseInt(id.split("_")[1]);
    const posX = parseInt(id.split("_")[2]);
    var cells = new Array(shipLength);
    var cellsListOnlyId = new Array(shipLength);
    let posOk = true;

    for (var x = 0; x < shipLength; x++){
        const cell = $("#fieldUser_" + posY + "_" + (posX + x));

        cells[x] = cell;
        cellsListOnlyId[x] = cell.attr("id");

        if (cell.text() === "x" ||
            cell.length === 0 ||
            $("#fieldUser_" + (posY + 1) + "_" + (posX + x)).text() !== "" ||
            $("#fieldUser_" + (posY - 1) + "_" + (posX + x)).text() !== "" ||
            $("#fieldUser_" + posY + "_" + (posX + x + 1)).text() !== "" ||
            $("#fieldUser_" + posY + "_" + (posX - 1)).text() !== "" ||
            $("#fieldUser_" + (posY + 1) + "_" + (posX - 1)).text() !== "" ||
            $("#fieldUser_" + (posY - 1) + "_" + (posX - 1)).text() !== "" ||
            $("#fieldUser_" + (posY + 1) + "_" + (posX + x + 1)).text() !== "" ||
            $("#fieldUser_" + (posY - 1) + "_" + (posX + x + 1)).text() !== ""){

            posOk = false;
        }

    }

    cells.forEach((cell) => {

        //console.log(cell);

        if(posOk){
            cell.css("background-color", "lightgreen");
        } else {
            cell.css("background-color", "red");
        }

    });

    if(posOk){

        return cellsListOnlyId;
    }

    return null;

}

/**
 * Generates the possible ship placement vertically on the user field based on the given ID and ship length.
 *
 * @param {string} id - The ID of the cell where the ship placement starts.
 * @param {number} shipLength - The length of the ship to be placed vertically.
 * @return {Array} An array containing the IDs of the cells that represent the possible ship placement vertically. Returns null if the ship placement is not possible.
 */
function showPossibleShipPlacementVertical(id, shipLength){

    const posY = parseInt(id.split("_")[1]);
    const posX = parseInt(id.split("_")[2]);
    var cellsVertical = new Array(shipLength);
    var cellsListOnlyIdVertical = new Array(shipLength);
    let posOk = true;


    for (var x = 0; x < shipLength; x++){
        const cell = $("#fieldUser_" + (posY + x) + "_" + posX);

        cellsVertical[x] = cell;
        cellsListOnlyIdVertical[x] = cell.attr("id");

        if (cell.text() === "x" ||
            cell.length === 0 ||
            $("#fieldUser_" + (posY + x + 1) + "_" + posX).text() !== "" ||
            $("#fieldUser_" + (posY - 1) + "_" + posX).text() !== "" ||
            $("#fieldUser_" + (posY + x) + "_" + (posX + 1)).text() !== "" ||
            $("#fieldUser_" + (posY + x) + "_" + (posX - 1)).text() !== "" ||
            $("#fieldUser_" + (posY - 1) + "_" + (posX - 1)).text() !== "" ||
            $("#fieldUser_" + (posY - 1) + "_" + (posX + 1)).text() !== "" ||
            $("#fieldUser_" + (posY + x + 1) + "_" + (posX + 1)).text() !== "" ||
            $("#fieldUser_" + (posY + x + 1) + "_" + (posX - 1)).text() !== ""){
            posOk = false;
        }

    }

    cellsVertical.forEach((cell) => {

        //console.log(cell);

        if(posOk){
            cell.css("background-color", "lightgreen");
        } else {
            cell.css("background-color", "red");
        }

    });

    if(posOk){

        return cellsListOnlyIdVertical;
    }

    return null;

}


/**
 * Cleans the possible ship placements by setting the background color of all player cells to white.
 *
 * @param {type} None
 * @return {type} None
 */
function cleanPossibleShipPlacement(){

    $('.player-cell').css('background-color', 'white');

}

function resetGame(){

    fetch(contextPath + '/game/resetPlayerField', {
        method: 'POST',
    })
        .then(response => response.text())
        .then(data => {

            var jsonResponse = JSON.parse(data);

            if(jsonResponse.status === 1){
                window.location.reload();
            }

        })
        .catch(error => {
            console.error('Error:', error);
        });

}

$(window).on("load", function () {
    var shipSize = 5;
    var isShipHorizontal = true;
    var timeout;

    $(window).keyup(function (e){
        if(e.keyCode === 53){
            shipSize = 5;
        } else if(e.keyCode === 52){
            shipSize = 4;
        } else if(e.keyCode === 51){
            shipSize = 3;
        } else if(e.keyCode === 50){
            shipSize = 2;
        } else if(e.keyCode === 82){
            isShipHorizontal = !isShipHorizontal;
        }
    });


    $(".player-cell").hover(function(){
        timeout = window.setTimeout(() => {

            if (isShipHorizontal){
                var cellsHorizontal = showPossibleShipPlacementHorizontal(this.id, shipSize);
                $(this).click(() => {
                    if(cellsHorizontal !== null && cellsHorizontal.length === shipSize && isShipHorizontal){
                        placeShip(cellsHorizontal);
                        console.log(cellsHorizontal);
                    }
                });

            } else {
                var cellsVertical = showPossibleShipPlacementVertical(this.id, shipSize);
                $(this).click(() => {
                    if(cellsVertical !== null && cellsVertical.length === shipSize && !isShipHorizontal){
                        placeShip(cellsVertical);
                        console.log(cellsVertical);
                    }
                });

            }

        }, 500);

    }, function(){

        window.clearTimeout(timeout);

        cleanPossibleShipPlacement();

    });

    // Show the how to play modal when the page is loaded
    $('#howToPlayModal').modal('show');
    $('#iconInstructionsModal').modal('show');


    $("#playMultiplayerP2Button").on("click", () => {
        window.location.href = contextPath + "/multiplayer/join/" + $("#multiplayerGameId").val() + "?gameId=" + $("#multiplayerGameId").data("gameid");
    });

});
