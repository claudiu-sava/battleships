var multiplayerGameId = document.cookie.split("multiplayerGameId=")[1].split(";")[0];
var contextPath = document.cookie.split("contextPath=")[1].split(";")[0];

if(contextPath === "/"){
    contextPath = "";
}


$(window).on("load", () => {

    updateFields();
    $('#howToPlayModalMultiplayer').modal('show');

});


function shootMultiplayer(id) {
    var shipY = id.split("_")[1];
    var shipX = id.split("_")[2];

    fetch(contextPath + '/multiplayer/shoot' + "?shipY=" + shipY + "&shipX=" + shipX + "&gameId=" + multiplayerGameId, {
        method: 'POST',
    })
        .then(response => response.text())
        .then(data => {

            var jsonResponse = JSON.parse(data);

            console.log(data);

            if(jsonResponse.status === 1){
                $('#fieldEnemy_' + jsonResponse.shipY + "_" + jsonResponse.shipX)
                    .removeClass("btn-outline-dark")
                    .addClass("btn-outline-danger")
                    .prop("disabled",true)
                    .find('img')
                    .attr('src', contextPath + "/drawable/ship_explosion.gif")
                    .removeAttr("hidden")
                    .addClass("cell");
            } else {
                $('#fieldEnemy_' + jsonResponse.shipY + "_" + jsonResponse.shipX)
                    .find('img')
                    .attr('src', contextPath + "/drawable/explosion.gif")
                    .removeAttr("hidden")
                    .addClass("cell");
            }

        })
        .catch(error => {
            console.error('Error:', error);
        });

}


function updateFields(){

    this.setTimeout(() => {

        var xhr = new XMLHttpRequest();
        xhr.open('GET', contextPath + '/multiplayer/update?gameId=' + multiplayerGameId, true);

        xhr.onload = function() {
            if (xhr.status >= 200 && xhr.status < 400) {
                // Parse the response (assuming it's HTML in this case)
                var fragmentContent = xhr.responseText;

                var fragmentContainer = document.getElementById('playerField');
                fragmentContainer.innerHTML = fragmentContent;

                if($("#ratioEnemyCounter").text() === "0.00"){
                    window.location.href = contextPath + '/game/gameOver?winner=Player2&score=' + $('#scoreCounter').text();
                } else if ($("#ratioPlayerCounter").text() === "0.00"){
                    window.location.href = contextPath + '/game/gameOver?winner=Player1&score=' + $('#scoreCounter').text();
                }

            } else {
                console.error('Request failed with status:', xhr.status);
            }
        };

        // Handle any errors
        xhr.onerror = function() {
            console.error('Request failed');
        };

        // Send the request

        window.setTimeout(() => {
            xhr.send();
        }, 100);

        updateScoreTable();
        updateFields();
    }, 1000);


}


function updateScoreTable(){

    var xhr = new XMLHttpRequest();
    xhr.open('GET', contextPath + '/multiplayer/updateScoreTable?gameId=' + multiplayerGameId, true);

    xhr.onload = function() {
        if (xhr.status >= 200 && xhr.status < 400) {
            // Parse the response (assuming it's HTML in this case)
            var fragmentContent = xhr.responseText;

            var fragmentContainer = document.getElementById('scoreTable');
            fragmentContainer.innerHTML = fragmentContent;

        } else {
            console.error('Request failed with status:', xhr.status);
        }
    };

    // Handle any errors
    xhr.onerror = function() {
        console.error('Request failed');
    };

    // Send the request

    window.setTimeout(() => {
        xhr.send();
    }, 100);

}
