package com.dtp.Battleships.service;

import org.springframework.stereotype.Service;
import com.dtp.Battleships.service.indexservice.Index;
import java.util.ArrayList;
import java.util.Random;
import java.util.LinkedList;


@Service
public class RandomService {

    private static LinkedList<Index> randomAccessLList;
    private static LinkedList<Index> randomAccessDiagonalLList;
    private static int randomAccessLListSize =0;
    private static int randomAccessDiagonalLListSize =0;

    private static boolean fieldAnalyzed= false; // state machine

    /**
     * Generates a random number within the specified maximum range.
     *
     * @param  maximum   the upper bound of the range for the random number
     * @return           the randomly generated number
     */
    public Integer generateRandomNumber(int maximum){
        Random random = new Random();
        return random.nextInt(maximum);
    }

    /**
     * Generates a random 5-digit PIN.
     *
     * @return         the generated random 5-digit PIN
     */
    public Integer generateRandom5DigitsPin(){
        Random r = new Random( System.currentTimeMillis() );
        return 10000 + r.nextInt(20000);
    }

    /**
     * Determines if the next field is changing row based on the current diagonal field and field size.
     *
     * @param  currDiagonalField   the current diagonal field
     * @param  fieldSize           the size of the field
     * @return                     true if the next field is changing row, false otherwise
     */
    private boolean isNextFieldChangingRow(int currDiagonalField, int fieldSize){
        if(determineRowFrom(currDiagonalField-1, fieldSize) == determineRowFrom(currDiagonalField+1,fieldSize)){
            return false;
        }
        else{
            return true;
        }
    }

    /**
     * Check if the given number is even.
     *
     * @param  numb  the number to check
     * @return       true if the number is even, false otherwise
     */
    private boolean isEven(int numb){
        return numb % 2 == 0;
    }

    /**
     * Check if the given number is odd.
     *
     * @param  numb  the number to check
     * @return       true if the number is odd, false otherwise
     */
    private boolean isOdd(int numb){
        return numb % 2 != 0;
    }

    /**
     * Determines the row from the current field index based on the field size.
     *
     * @param  currField  the current field index
     * @param  fieldSize  the size of the field
     * @return           the row determined from the current field index
     */
    private int determineRowFrom(int currField, int fieldSize){
        return currField / fieldSize;
    }

    /**
     * Toggles the next additive based on the current diagonal field.
     *
     * @param  currDiagonalField  the current diagonal field
     * @return                   the toggled additive value
     */
    private int toggleNextAdditive(int currDiagonalField){
        if(isEven(currDiagonalField)){
            return 1;
        }
        else{
            return 3;
        }
    }

    /**
     * Identify the next diagonal field based on the current diagonal field and field size.
     *
     * @param  currDiagonalField  the current diagonal field
     * @param  fieldSize          the size of the field
     * @return                   the identified next diagonal field
     */
    private int identifyNextDiagonalField(int currDiagonalField, int fieldSize){
        int currNextAdditive = toggleNextAdditive(currDiagonalField);
        if(isNextFieldChangingRow(currDiagonalField, fieldSize)){
            return currDiagonalField + currNextAdditive;
        }
        else{
            return currDiagonalField +2;
        }
    }

    /**
     * Calculate the next diagonal field for even numbers.
     *
     * @param  currDiagonalField  the current diagonal field
     * @param  fieldSize          the size of the field
     * @return                   the next diagonal field
     */
    private int calculateNextDiagonalFieldForEven(int currDiagonalField, int fieldSize){
        return identifyNextDiagonalField(currDiagonalField, fieldSize);
    }

    /**
     * Calculate the next diagonal field for odd numbers.
     *
     * @param  currDiagonalField  the current diagonal field
     * @return                   the next diagonal field
     */
    private int calculateNextDiagonalFieldForOdd(int currDiagonalField){
        return currDiagonalField+2;
    }

    /**
     * Creates an ArrayList of LinkedLists for each cell and diagonal in the field.
     *
     * @param  fieldSize  the size of the field
     * @return           the ArrayList containing the LinkedLists
     */
    private ArrayList<LinkedList<Index>> createPerCellAndDiagonalLL(int fieldSize){
        ArrayList<LinkedList<Index>> LListPacket = new ArrayList<>(2);
        LinkedList<Index> perCellLList = new LinkedList<Index>();
        LinkedList<Index> diagonalLList = new LinkedList<Index>();
        Index currIndex = null;
        int currPerCellLListSize = 1;
        int nextDiagonalField = 2;

        for(int row =0; row< fieldSize; row++){
            for(int col = 0; col< fieldSize; col++){
                currIndex = new Index(row, col);
                perCellLList.add(currIndex);
                currPerCellLListSize = perCellLList.size();

                if(isEven(fieldSize)){
                    if(currPerCellLListSize == nextDiagonalField){
                        diagonalLList.add(currIndex);
                        nextDiagonalField = calculateNextDiagonalFieldForEven(nextDiagonalField,fieldSize);

                    }
                }

                else if(isOdd(fieldSize)){
                    if(currPerCellLListSize == nextDiagonalField){
                        diagonalLList.add(currIndex);
                        nextDiagonalField = calculateNextDiagonalFieldForOdd(nextDiagonalField);
                    }//Done
                }

            }
        }
        LListPacket.add(0,perCellLList);
        LListPacket.add(1,diagonalLList);
        return LListPacket;
    }

    /**
     * Generates a super smart random index based on the field size.
     *
     * @param  fieldSize   the size of the field
     * @return              the super smart random index
     */
    public Index generateSuperSmartRandomIndex(int fieldSize){

        if(!fieldAnalyzed){
            ArrayList<LinkedList<Index>> LListPacket = createPerCellAndDiagonalLL(fieldSize);
            randomAccessLList = LListPacket.get(0);
            randomAccessDiagonalLList = LListPacket.get(1);

            randomAccessLListSize = randomAccessLList.size();
            randomAccessDiagonalLListSize = randomAccessDiagonalLList.size();

            fieldAnalyzed = true;

        }
        Random random = new Random();
        int randomSmartIndex = random.nextInt(randomAccessDiagonalLListSize);
        return randomAccessDiagonalLList.get(randomSmartIndex);
    }

    /**
     * Reset the smart random generation, clearing the random access linked lists and setting sizes to 0.
     */
    public void resetSmartRandomGeneration(){
        if(randomAccessLList != null){
            randomAccessLList.clear();
        }
        if(randomAccessDiagonalLList != null){
            randomAccessDiagonalLList.clear();
        }
        randomAccessLListSize =0;
        randomAccessDiagonalLListSize =0;
        fieldAnalyzed = false;
    }

    /**
     * Generates a smart random index based on the field size.
     *
     * @param  fieldSize  the size of the field
     * @return           the smart random index
     */
    public Index generateSmartRandomIndex(int fieldSize){

        if(!fieldAnalyzed){
            randomAccessLList = createSingleLLPerFieldRow(fieldSize);
            randomAccessLListSize = randomAccessLList.size();
            fieldAnalyzed = true;
        }

        Random random = new Random();
        int randomIndex = random.nextInt(randomAccessLListSize);
        return randomAccessLList.get(randomIndex);
    }

    /**
     * Creates a linked list of Index objects for each row in the field.
     *
     * @param  fieldSize  the size of the field
     * @return           a linked list of Index objects
     */
    private LinkedList<Index> createSingleLLPerFieldRow(int fieldSize){
        LinkedList<Index> list = new LinkedList<Index>();

        for(int row =0; row< fieldSize; row++){
            for(int col = 0; col< fieldSize; col++){
                list.add(new Index(row, col));
            }

        }
        return list;
    }

    /**
     * Removes the specified index from the list based on the given who parameter.
     *
     * @param  index  the index to be removed from the list
     * @param  who    the type of removal operation to be performed ("Normal" or "Smart")
     */
    public void removeIndex(Index index, String who){
        if(who.equals("Normal")){
            randomAccessLList.remove(index);
            randomAccessLListSize = randomAccessLList.size();
        }
        else if(who.equals("Smart")){
            randomAccessLList.remove(index);
            randomAccessLListSize = randomAccessLList.size();
            randomAccessDiagonalLList.remove(index);
            randomAccessDiagonalLListSize = randomAccessDiagonalLList.size();
        }

    }

    /**
     * Retrieves the random access linked list.
     *
     * @return         	the random access linked list
     */
    public LinkedList<Index> getRandomAccessLList() {
        return randomAccessLList;
    }
}
