package com.dtp.Battleships.service;

import com.dtp.Battleships.model.MultiplayerGame;
import com.dtp.Battleships.repository.MultiplayerGameRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Optional;


@Service
public class MultiplayerGameService {


    @Autowired
    private MultiplayerGameRepository multiplayerGameRepository;
    @Autowired
    private GameService gameService;

    /**
     * Retrieves a multiplayer game by its ID.
     *
     * @param  id   the ID of the multiplayer game to retrieve
     * @return      the multiplayer game with the specified ID, or null if not found
     */
    public MultiplayerGame getMultiplayerGameById(int id){


        Optional<MultiplayerGame> multiplayerGame = multiplayerGameRepository.findById(id);
        if(multiplayerGame.isPresent()){
            return multiplayerGame.get();
        } else {
            return null;
        }

    }

    /**
     * Saves a multiplayer game.
     *
     * @param  multiplayerGame	The multiplayer game to be saved
     * @return         		void
     */
    public void saveMultiplayerGame(MultiplayerGame multiplayerGame){

        multiplayerGameRepository.save(multiplayerGame);

    }

    /**
     * Shoots at the enemy's field and updates the game state based on the result.
     *
     * @param  shipY    the y-coordinate of the ship to shoot
     * @param  shipX    the x-coordinate of the ship to shoot
     * @param  game     the multiplayer game instance
     * @param  whoAmI   the identifier of the player shooting
     * @return          the result of the shot
     */
    public HashMap<String, Integer> shoot(int shipY, int shipX, MultiplayerGame game, String whoAmI){

        String[][] enemyField;

        // See if the player is allowed to shoot (is his turn)
        if (whoAmI.equals("p1") && game.getTurn() == 1){
            enemyField = gameService.fieldStringToArray(game.getPlayerTwo());
        } else if (whoAmI.equals("p2") && game.getTurn() == 2){
            enemyField = gameService.fieldStringToArray(game.getPlayerOne());
        } else {
            return null;
        }

        HashMap<String, Integer> result = gameService.shoot(shipY, shipX, enemyField);

        enemyField[shipY][shipX] += "O";

        // if shot was a hit let player who shoot to shoot again
        if(result.get("status").equals(1)){
            if(whoAmI.equals("p1")){
                game.setPlayerOneHits(game.getPlayerOneHits() + 1);
                game.setPlayerTwo(Arrays.deepToString(enemyField));
            } else{
                game.setPlayerTwoHits(game.getPlayerTwoHits() + 1);
                game.setPlayerOne(Arrays.deepToString(enemyField));
            }

        // if shot was a miss change turn
        } else {
            if(whoAmI.equals("p1")){
                game.setPlayerTwo(Arrays.deepToString(enemyField));
                game.setPlayerOneMisses(game.getPlayerOneMisses() + 1);
                game.setTurn(2);
            } else{
                game.setPlayerOne(Arrays.deepToString(enemyField));
                game.setPlayerTwoMisses(game.getPlayerTwoMisses() + 1);
                game.setTurn(1);
            }
        }

        saveMultiplayerGame(game);

        return result;

    }

    /**
     * Calculate the ratio of hit cells to total cells in the given field.
     *
     * @param  field  2D array representing the field of cells
     * @return       the ratio of hit cells to total cells
     */
    public Double calculateRatio(String[][] field){

        double totalCells = 0;
        double hitCells = 0;

        for(String[] row: field){
            for(String cell: row){
                if(cell.contains("xO")){
                    hitCells++;
                }
                if(cell.contains("x")){
                    totalCells++;
                }
            }
        }

        return 100 - ((100 / totalCells) * hitCells);

    }

}
