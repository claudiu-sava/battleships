package com.dtp.Battleships.service;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
public class SessionService {

    /**
     * Retrieves the logged username.
     *
     * @return the name of the authenticated user
     */
    public String getLoggedUsername(){

        return SecurityContextHolder.getContext().getAuthentication().getName();

    }

}
