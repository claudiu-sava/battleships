package com.dtp.Battleships.service;

import com.dtp.Battleships.model.Game;
import com.dtp.Battleships.repository.GameRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Optional;

@Service
public class GameService {

    @Autowired
    private AIService aiService;
    @Autowired
    private GameRepository gameRepository;
    @Autowired
    private RandomService randomService;

    /**
     * Saves the given game using the game repository.
     *
     * @param  game  the game to be saved
     */
    public void saveGame(Game game){

        gameRepository.save(game);

    }

    /**
     * Cleans the input field by removing occurrences of "O"
     * and reducing consecutive repeated characters to two.
     *
     * @param  field  the input field to be cleaned
     * @return        the cleaned field
     */
    public String cleanField(String field){

        String cleanedField = field
                .replace("O", "")
                .replaceAll("(?s)(.)\\1+", "$1$1");

        return cleanedField;
    }

    /**
     * Cleans the player and enemy fields of the given Game object and saves the game.
     *
     * @param  game  the Game object to be cleaned
     * @return       void
     */
    public void cleanGame(Game game){
        String cleanedPlayerField = cleanField(game.getPlayerField());
        String cleanedEnemyField = cleanField(game.getEnemyField());

        game.setPlayerField(cleanedPlayerField);
        game.setEnemyField(cleanedEnemyField);
        saveGame(game);
    }

    /**
     * Resets the player field of the game.
     *
     * @param  game  the game object
     */
    public void resetPlayerField(Game game){

        game.setPlayerField(game.getPlayerField().replaceAll("x", "null"));
        saveGame(game);

    }

    /**
     * Retrieves a Game object by its ID.
     *
     * @param  id  the ID of the game to retrieve
     * @return     the Game object with the specified ID, or null if no game is found
     */
    public Game getGameById(int id){

        Optional<Game> game = gameRepository.findById(id);
        if(game.isPresent()){
            return game.get();
        } else {
            return null;
        }

    }

    /**
     * Converts a string representation of a 2D array into an actual 2D array.
     *
     * @param  fieldString   the string representation of the 2D array
     * @return               the converted 2D array
     */
    public String[][] fieldStringToArray(String fieldString){

        return Arrays.stream(fieldString.substring(1, fieldString.length() - 1).split("],"))
                .map(String::trim)
                .map(row -> Arrays.stream(row.split(","))
                        .map(str -> str.trim().replaceAll("\"", ""))
                        .toArray(String[]::new))
                .toArray(String[][]::new);

    }

    /**
     * Places a ship on the game board.
     *
     * @param shipName the name of the ship to be placed
     * @param shipY the y-coordinate of the ship's position
     * @param shipX the x-coordinate of the ship's position
     * @return 0 if there was an error, 1 if the cell was successfully placed, 2 if the cell was removed
     */
    public int placeShip(String shipName, int shipY, int shipX, String[][] playerField){
        // 0 - error
        // 1 - cell placed
        // 2 - cell removed

        int out = 0;

        if(playerField[shipY][shipX].contains("x")){
            playerField[shipY][shipX] = null;
            out = 2;
        } else {
            playerField[shipY][shipX] = shipName;
            out = 1;
        }

        return out;

    }

    /**
     * A function to shoot at a specific location on the field.
     *
     * @param  shipY   the Y coordinate of the ship
     * @param  shipX   the X coordinate of the ship
     * @param  field   the 2D array representing the field
     * @return         a HashMap containing the updated status of the shot
     */
    public HashMap<String, Integer> shoot(int shipY, int shipX, String[][] field){

        HashMap<String, Integer> response = new HashMap<>();
        response.put("shipY", shipY);
        response.put("shipX", shipX);

        if(shipY < 0 || shipX < 0){
            response.put("status", -1);
            return response;
        }

        // The order is VERY IMPORTANT!
          if (field[shipY][shipX].contains("O")){
              response.put("status", 2);
          } else if(field[shipY][shipX].contains("x")) {
              response.put("status", 1);
          } else {
              response.put("status", 0);
        }

        return response;

    }

    /**
     * Calculates the ratio of cells containing "x" in the given field.
     *
     * @param  field  a 2D array representing the field
     * @return       the calculated ratio
     */
    public Double calculateRatio(String[][] field){
        double totalCells = 0;

        for(String[] row: field){
            for(String cell: row){
                if(cell.contains("x")){
                    totalCells++;
                }
            }
        }
        return (100 / totalCells) * 1;
    }


    /**
     * Shoots back at the specified game with the given player.
     *
     * @param  gameId   the ID of the game
     * @param  who      the player shooting back
     * @return          the response HashMap
     */
    public HashMap<String, Integer> shootBack(int gameId, String who){
        Game game = getGameById(gameId);
        String[][] field = fieldStringToArray(game.getPlayerField());
        HashMap<String, Integer>response = aiService.shootBack(field, who);
        game.setPlayerField(Arrays.deepToString(field));
        saveGame(game);

        return response;
    }

    /**
     * Shoots back at the hacker in the game.
     *
     * @param  gameId    the ID of the game
     * @return          the response after shooting
     */
    public HashMap<String, Integer> shootBackHacker(int gameId){

        Game game = getGameById(gameId);
        String[][] field = fieldStringToArray(game.getPlayerField());

        int rows = 0;
        int shipY = -1;
        int shipX = -1;

        for (String[] row : field){
            int cols = 0;
            for (String cell : row){

                if(cell.contains("x") && !cell.contains("O")){

                    shipY = rows;
                    shipX = cols;

                }
                cols += 1;
            }
            rows += 1;
        }

        HashMap<String, Integer> response = new HashMap<>();

        do {
            response = shoot(shipY, shipX, field);

            if (response.get("status") == 2) {
                shipY = randomService.generateRandomNumber(field.length);
                shipX = randomService.generateRandomNumber(field.length);
            }
        } while (response.get("status") == 2);

        if(response.get("status") == -1) {
            System.out.println("Hacker mode has shot evey ship. Game over.");
            return response;
        }

        field[response.get("shipY")][response.get("shipX")] += "O";
        game.setPlayerField(Arrays.deepToString(field));
        saveGame(game);

        return response;
    }

    /**
     * Retrieves the ID of the game.
     *
     * @param  game  the game object
     * @return       the ID of the game
     */
    public int getGameId(Game game){

        return game.getId();

    }

    /**
     * Checks the size of the field and ensures it is within a valid range.
     *
     * @param  fieldSize  the size of the field to be checked
     * @return            the validated field size within a valid range
     */
    public int checkFieldSize(int fieldSize){
        if (fieldSize > 15) {
            fieldSize = 15;
        } else if (fieldSize < 12) {
            fieldSize = 12;
        }

        return fieldSize;
    }




}
