package com.dtp.Battleships.service;

import com.dtp.Battleships.model.User;
import com.dtp.Battleships.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserService {
    @Autowired
    private UserRepository userRepository;

    /**
     * Retrieves a user by their username.
     *
     * @param  username   the username of the user to retrieve
     * @return            the user with the specified username, or null if not found
     */
    public User getUserByUsername(String username){
        Optional<User> userOptional = userRepository.findByUsername(username);

        if(userOptional.isPresent()){
            return userOptional.get();
        }

        return null;

    }

    /**
     * Saves the user to the repository.
     *
     * @param  user  the user to be saved
     */
    public void saveUser(User user){

        userRepository.save(user);

    }

}
