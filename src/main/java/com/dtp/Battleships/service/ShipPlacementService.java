package com.dtp.Battleships.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;

@Service
public class ShipPlacementService {

    @Autowired
    private RandomService randomService;

    /**
     * Check if a ship of given size can be placed to the right of the starting position
     *
     * @param  shipSize    the size of the ship
     * @param  field       the game field
     * @param  startingY   the starting Y coordinate
     * @param  startingX   the starting X coordinate
     *      * @return a HashMap containing information about the validity of the ship placement:
     *      * - "canBePlaced": a boolean indicating if the ship can be placed to the left
     *      * - "borderUp": a boolean indicating if there is a border above the ship
     *      * - "borderDown": a boolean indicating if there is a border below the ship
     *      * - "borderLeft": a boolean indicating if there is a border to the left of the ship
     *      * - "borderRight": a boolean indicating if there is a border to the right of the ship
     **/
    public HashMap<String, Boolean> checkRight(int shipSize, String[][] field, int startingY, int startingX) {
        HashMap<String, Boolean> out = new HashMap<>();

        // Check if the ship would go out of bounds to the right
        if (startingX + shipSize - 1 >= field.length) {
            out.put("canBePlaced", false);
            return out;
        }

        // Check if any of the positions to the right of the starting position are already occupied
        for (int x = 0; x < shipSize; x++) {
            if (field[startingY][startingX + x] != null) {
                out.put("canBePlaced", false);
                return out;
            }

            // Check if any of the positions above the ship are occupied (border)
            if (startingY > 0) {
                if (field[startingY - 1][startingX + x] == null) {
                    out.put("borderUp", true);
                } else {
                    out.put("canBePlaced", false);
                    return out;
                }

            } else {
                out.put("borderUp", false);
            }

            // Check if any of the positions below the ship are occupied (border)
            if (startingY < field.length - 1) {
                if (field[startingY + 1][startingX + x] == null) {
                    out.put("borderDown", true);
                } else {
                    out.put("canBePlaced", false);
                    return out;
                }
            } else {
                out.put("borderDown", false);
            }
        }

        // Check if the left position of the ship is occupied (border)
        if (startingX >= 1) {
            if (field[startingY][startingX - 1] == null) {
                out.put("borderLeft", true);
            } else {
                out.put("canBePlaced", false);
                return out;
            }
        } else {
            out.put("borderLeft", false);
        }

        // Check if the right position of the ship is occupied (border)
        if (startingX + shipSize - 1 < field.length -1) {
            if (field[startingY][startingX + shipSize] == null) {
                out.put("borderRight", true);
            } else {
                out.put("canBePlaced", false);
                return out;
            }
        } else {
            out.put("borderRight", false);
        }

        // The ship can be placed to the right
        out.put("canBePlaced", true);
        return out;
    }


    /**
     * Checks if a ship of a given size can be placed to the left of a starting position on the game field.
     *
     * @param shipSize  the size of the ship to be placed
     * @param field     the game field represented as a 2D array
     * @param startingY the Y coordinate of the starting position
     * @param startingX the X coordinate of the starting position
     * @return a HashMap containing information about the validity of the ship placement:
     * - "canBePlaced": a boolean indicating if the ship can be placed to the left
     * - "borderUp": a boolean indicating if there is a border above the ship
     * - "borderDown": a boolean indicating if there is a border below the ship
     * - "borderLeft": a boolean indicating if there is a border to the left of the ship
     * - "borderRight": a boolean indicating if there is a border to the right of the ship
     */
    public HashMap<String, Boolean> checkLeft(int shipSize, String[][] field, int startingY, int startingX) {
        HashMap<String, Boolean> out = new HashMap<>();

        // Check if the ship would go out of bounds to the left
        if (startingX - shipSize + 1 < 0) {
            out.put("canBePlaced", false);
            return out;
        }

        // Check if any of the positions to the left of the starting position are already occupied
        for (int x = 0; x < shipSize; x++) {
            if (field[startingY][startingX - x] != null) {
                out.put("canBePlaced", false);
                return out;
            }

            // Check if any of the positions above the ship are occupied (border)
            if (startingY > 0) {
                if (field[startingY - 1][startingX - x] == null) {
                    out.put("borderUp", true);
                } else {
                    out.put("canBePlaced", false);
                    return out;
                }
            } else {
                out.put("borderUp", false);
            }

            // Check if any of the positions below the ship are occupied (border)
            if (startingY < field.length - 1) {
                if (field[startingY + 1][startingX - x] == null) {
                    out.put("borderDown", true);
                } else {
                    out.put("canBePlaced", false);
                    return out;
                }
            } else {
                out.put("borderDown", false);
            }
        }

        // Check if the left position of the ship is occupied (border)
        if (startingX - shipSize + 1 > 0) {
            if(field[startingY][startingX - shipSize] == null){
                out.put("borderLeft", true);
            } else {
                out.put("canBePlaced", false);
                return out;
            }
        } else {
            out.put("borderLeft", false);
        }

        // Check if the right position of the ship is occupied (border)
        if (startingX < field.length - 1) {
            if (field[startingY][startingX + 1] == null) {
                out.put("borderRight", true);
            } else {
                out.put("canBePlaced", false);
                return out;
            }
        } else {
            out.put("borderRight", false);
        }

        // The ship can be placed to the left
        out.put("canBePlaced", true);
        return out;


    }


    /**
     * Checks if a ship of given size can be placed upwards from a starting position on a field.
     *
     * @param shipSize  the size of the ship to be placed
     * @param field     the field where the ship is to be placed
     * @param startingY the Y coordinate of the starting position
     * @param startingX the X coordinate of the starting position
     * @return a HashMap containing the following information:
     * - "canBePlaced": a boolean indicating if the ship can be placed
     * - "borderUp":    a boolean indicating if the up position is a border
     * - "borderDown":  a boolean indicating if the down position is a border
     * - "borderLeft":  a boolean indicating if the left position is a border
     * - "borderRight": a boolean indicating if the right position is a border
     */
    public HashMap<String, Boolean> checkUp(int shipSize, String[][] field, int startingY, int startingX) {
        HashMap<String, Boolean> out = new HashMap<>();

        // Check if the ship would go out of bounds to the up
        if (startingY - shipSize + 1 < 0) {
            out.put("canBePlaced", false);
            return out;
        }

        // Check if any of the positions to the up of the starting position are already occupied
        for (int y = 0; y < shipSize; y++) {
            if (field[startingY - y][startingX] != null) {
                out.put("canBePlaced", false);
                return out;
            }

            // Check if any of the positions to the left of the ship are occupied (border)
            if (startingX > 0) {
                if (field[startingY - y][startingX - 1] == null) {
                    out.put("borderLeft", true);
                } else {
                    out.put("canBePlaced", false);
                    return out;
                }
            } else {
                out.put("borderLeft", false);
            }

            // Check if any of the positions to the right of the ship are occupied (border)
            if (startingX < field.length - 1) {
                if (field[startingY - y][startingX + 1] == null) {
                    out.put("borderRight", true);
                } else {
                    out.put("canBePlaced", false);
                    return out;
                }
            } else {
                out.put("borderRight", false);
            }
        }

        // Check if the up position of the ship is occupied (border)
        if (startingY - shipSize + 1 > 0) {
            if (field[startingY - shipSize][startingX] == null) {
                out.put("borderUp", true);
            } else {
                out.put("canBePlaced", false);
                return out;
            }
        } else {
            out.put("borderUp", false);
        }

        // Check if the down position of the ship is occupied (border)
        if (startingY < field.length - 1) {
            if (field[startingY + 1][startingX] == null) {
                out.put("borderDown", true);
            } else {
                out.put("canBePlaced", false);
                return out;
            }
        } else {
            out.put("borderDown", false);
        }

        // The ship can be placed to the up
        out.put("canBePlaced", true);
        return out;

    }

    /**
     * Checks whether a ship of a given size can be placed in the down direction from a starting position on the field.
     *
     * @param  shipSize      the size of the ship to be checked
     * @param  field         the 2D array representing the field
     * @param  startingY     the starting Y coordinate of the ship
     * @param  startingX     the starting X coordinate of the ship
     * @return               a HashMap containing the following keys:
     *                       - "canBePlaced": a boolean indicating whether the ship can be placed
     *                       - "borderLeft": a boolean indicating whether the left border is occupied
     *                       - "borderRight": a boolean indicating whether the right border is occupied
     *                       - "borderUp": a boolean indicating whether the up border is occupied
     *                       - "borderDown": a boolean indicating whether the down border is occupied
     */
    public HashMap<String, Boolean> checkDown(int shipSize, String[][] field, int startingY, int startingX) {
        HashMap<String, Boolean> out = new HashMap<>();

        // Check if the ship would go out of bounds to the down
        if (startingY + shipSize - 1 >= field.length) {
            out.put("canBePlaced", false);
            return out;
        }

        // Check if any of the positions to the down of the starting position are already occupied
        for (int y = 0; y < shipSize; y++) {
            if (field[startingY + y][startingX] != null) {
                out.put("canBePlaced", false);
                return out;
            }

            // Check if any of the positions to the left of the ship are occupied (border)
            if (startingX > 0) {
                if (field[startingY + y][startingX - 1] == null) {
                    out.put("borderLeft", true);
                } else {
                    out.put("canBePlaced", false);
                    return out;
                }
            } else {
                out.put("borderLeft", false);
            }

            // Check if any of the positions to the right of the ship are occupied (border)
            if (startingX < field.length - 1) {
                if (field[startingY + y][startingX + 1] == null) {
                    out.put("borderRight", true);
                } else {
                    out.put("canBePlaced", false);
                    return out;
                }
            } else {
                out.put("borderRight", false);
            }
        }

        // Check if the up position of the ship is occupied (border)
        if (startingY - 1 >= 0) {
            if (field[startingY - 1][startingX] == null) {
                out.put("borderUp", true);
            } else {
                out.put("canBePlaced", false);
                return out;
            }
        } else {
            out.put("borderUp", false);
        }

        // Check if the down position of the ship is occupied (border)
        if (startingY + shipSize < field.length) {
            if (field[startingY + shipSize - 1][startingX] == null) {
                out.put("borderDown", true);
            } else {
                out.put("canBePlaced", false);
                return out;
            }
        } else {
            out.put("borderDown", false);
        }

        out.put("canBePlaced", true);
        return out;
    }

    /**
     * Places a ship of a given size on the field according to the specified rules.
     *
     * @param  shipSize    the size of the ship to be placed
     * @param  field       the 2D array representing the game field
     * @return             the updated field with the ship placed
     */
    public String[][] placeShip(int shipSize, String[][] field) {

        int startingX = randomService.generateRandomNumber(field.length);
        int startingY = randomService.generateRandomNumber(field.length);

        HashMap<String, Boolean> checkRight = checkRight(shipSize, field, startingY, startingX);
        HashMap<String, Boolean> checkLeft = checkLeft(shipSize, field, startingY, startingX);
        HashMap<String, Boolean> checkUp = checkUp(shipSize, field, startingY, startingX);
        HashMap<String, Boolean> checkDown = checkDown(shipSize, field, startingY, startingX);

        // place the ship after checking the position

        // check if the ship can be placed to the right
        if (checkRight.get("canBePlaced")) {

            for (int x = 0; x < shipSize; x++) {

                // place the ship
                field[startingY][startingX + x] = "x";

                // place the border above the ship
                if (checkRight.get("borderUp")) {
                    field[startingY - 1][startingX + x] = "b";
                }

                // place the border below the ship
                if (checkRight.get("borderDown")) {
                    field[startingY + 1][startingX + x] = "b";
                }

                // place the border to the left of the ship
                if (checkRight.get("borderLeft")) {
                    field[startingY][startingX - 1] = "b";
                }

                // place the border to the right of the ship
                if (checkRight.get("borderRight")) {
                    field[startingY][startingX + shipSize] = "b";
                }

            }

            // check if the ship can be placed to the left
        } else if (checkLeft.get("canBePlaced")) {
            for (int x = 0; x < shipSize; x++) {

                // place the ship
                field[startingY][startingX - x] = "x";

                // place the border above the ship
                if (checkLeft.get("borderUp")) {
                    field[startingY - 1][startingX - x] = "b";
                }

                // place the border below the ship
                if (checkLeft.get("borderDown")) {
                    field[startingY + 1][startingX - x] = "b";
                }

                // place the border to the left of the ship
                if (checkLeft.get("borderLeft")) {
                    field[startingY][startingX - shipSize] = "b";
                }

                // place the border to the right of the ship
                if (checkLeft.get("borderRight")) {
                    field[startingY][startingX + 1] = "b";
                }

            }
        } else if (checkUp.get("canBePlaced")) {
            for (int y = 0; y < shipSize; y++) {

                // place the ship
                field[startingY - y][startingX] = "x";

                // place the border above the ship
                if (checkUp.get("borderUp")) {
                    field[startingY - shipSize][startingX] = "b";
                }

                // place the border below the ship
                if (checkUp.get("borderDown")) {
                    field[startingY + 1][startingX] = "b";
                }

                // place the border to the left of the ship
                if (checkUp.get("borderLeft")) {
                    field[startingY - y][startingX - 1] = "b";
                }

                // place the border to the right of the ship
                if (checkUp.get("borderRight")) {
                    field[startingY - y][startingX + 1] = "b";
                }

            }
        } else if(checkDown.get("canBePlaced")) {
            for (int y = 0; y < shipSize; y++) {

                // place the ship
                field[startingY + y][startingX] = "x";

                // place the border above the ship
                if (checkDown.get("borderUp")) {
                    field[startingY - 1][startingX] = "b";
                }

                // place the border below the ship
                if (checkDown.get("borderDown")) {
                    field[startingY + shipSize][startingX] = "b";
                }

                // place the border to the left of the ship
                if (checkDown.get("borderLeft")) {
                    field[startingY + y][startingX - 1] = "b";
                }

                // place the border to the right of the ship
                if (checkDown.get("borderRight")) {
                    field[startingY + y][startingX + 1] = "b";
                }

            }
        } else {
            System.out.println("There are no more ship placement possibilities, going back");
            placeShip(shipSize, field);
        }

        return field;
    }

}
