package com.dtp.Battleships.service;

import com.dtp.Battleships.service.indexservice.Index;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.LinkedList;

@Service
public class AIService {

    @Autowired
    private RandomService randomService;

    private static LinkedList<Index> shipIndices = new LinkedList<>();

    /**
     * Shoots at the player field based on the given strategy.
     *
     * @param  playerField  the 2D array representing the player's field
     * @param  who          the strategy to use for shooting ("Normal" or "Smart")
     * @return              the index to shoot at
     */
    private Index shoot(String[][] playerField, String who){
        if(who.equals("Easy")){
            return randomService.generateSmartRandomIndex(playerField.length);
        }
        if(who.equals("Normal")){
            return randomService.generateSmartRandomIndex(playerField.length);
        }
        else if (who.equals("Smart")) {
            return randomService.generateSuperSmartRandomIndex(playerField.length);
        }

        return null;
    }

    /**
     * Removes the specified index from the linked list.
     *
     * @param  index  the index to be removed
     * @param  who    the identifier of the action performer
     */
    private void removeIndexFromLL(Index index, String who){
        randomService.removeIndex(index, who);
    }

    /**
     * Checks if a ship is found at the given index.
     *
     * @param  shipShot   the index to check for a ship
     * @return           true if a ship is found, false otherwise
     */
    private Boolean foundShip(Index shipShot){
        return shipShot != null;
    }

    /**
     * Extracts the next ship part from the random access linked list based on the given row and column.
     *
     * @param  nextRow    the row of the next ship part
     * @param  nextCol    the column of the next ship part
     * @return           the next ship part from the random access linked list, or null if not found
     */
    private Index extractNextShipPartFromRandomAccessLL(int nextRow, int nextCol){
        LinkedList<Index> list = randomService.getRandomAccessLList();
        Index NextShipPart = null;

        for(Index index : list){
            if(index.getRow() == nextRow && index.getCol() == nextCol){
                NextShipPart = index;
            }

        }
        return NextShipPart;
    }

    /**
     * Search for a ship in the player field based on the current index.
     *
     * @param  currIndex    the current index to start the search from
     * @param  playerField  the player's field containing ship positions
     * @return              the next ship part index if found, otherwise null
     */
    private Index searchShip(Index currIndex, String[][] playerField){
        int currRow = currIndex.getRow();
        int currCol = currIndex.getCol();

        if(0<= currRow-1 ){ //Top
            if(playerField[currRow-1][currCol].contains("x") && !playerField[currRow-1][currCol].contains("O")) {
                return extractNextShipPartFromRandomAccessLL(currRow-1, currCol);

            }
        }
        if(currRow+1< playerField.length){ // Bot
            if(playerField[currRow+1][currCol].contains("x") && !playerField[currRow+1][currCol].contains("0")) {
                return extractNextShipPartFromRandomAccessLL(currRow+1, currCol);
            }
        }
        if(currCol+1 < playerField.length){ // Right
            if(playerField[currRow][currCol+1].contains("x") && !playerField[currRow][currCol+1].contains("O")) {
                return extractNextShipPartFromRandomAccessLL(currRow, currCol+1);
            }
        }
        if(0<= currCol-1){// Left
            if(playerField[currRow][currCol-1].contains("x") && !playerField[currRow][currCol-1].contains("O")) {
                return extractNextShipPartFromRandomAccessLL(currRow, currCol-1);
            }
        }
        return null;
    }

    /**
     * A method to perform a smart shoot in the player field.
     *
     * @param  playerField  the 2D array representing the player's field
     * @return              the Index representing the smart shot
     */
    private Index smartShoot(String[][] playerField){

        Index startShot = shipIndices.getFirst();
        Index lastShot = shipIndices.getLast();

        Index shipShot = searchShip(lastShot, playerField);
        if(foundShip(shipShot)){
            shipIndices.addLast(shipShot);
            return shipShot;
        }

        shipShot = searchShip(startShot, playerField);
        if(foundShip(shipShot)){
            shipIndices.addFirst(shipShot);
        }
        return shipShot;

    }

    /**
     * A method to shoot back in the game based on playerField and who is shooting.
     *
     * @param  playerField  the 2D array representing the player's field
     * @param  who          the player who is shooting
     * @return              a HashMap containing the status and coordinates of the shot
     */
    public HashMap<String, Integer> shootBack(String[][] playerField, String who){
        int randRow = 0;
        int randCol = 0;
        Index shootedIndex = null;
        HashMap<String, Integer> response = new HashMap<>();

        if(shipIndices.isEmpty()){
            shootedIndex = shoot(playerField, who);
            randRow = shootedIndex.getRow();
            randCol = shootedIndex.getCol();

            if(playerField[randRow][randCol].contains("x") && !playerField[randRow][randCol].contains("O")){
                if(!who.equals("Easy")) {
                    shipIndices.addLast(new Index(randRow, randCol));
                }
                response.put("status", 1);
                response.put("shipY", shootedIndex.getRow());
                response.put("shipX", shootedIndex.getCol());
            }
            else{
                response.put("status", 0);
                response.put("shipY", shootedIndex.getRow());
                response.put("shipX", shootedIndex.getCol());

            }
            playerField[response.get("shipY")][response.get("shipX")] += "O";
            removeIndexFromLL(shootedIndex, who);
        }

        else if(!shipIndices.isEmpty()){
            shootedIndex = smartShoot(playerField); // returns null if all ships parts found else return Index Object
            if(foundShip(shootedIndex)) {
                response.put("status", 1);
                response.put("shipY", shootedIndex.getRow());
                response.put("shipX", shootedIndex.getCol());
                playerField[response.get("shipY")][response.get("shipX")] += "O";
                removeIndexFromLL(shootedIndex,who);

            }
            else {
                shipIndices.clear();
                return shootBack(playerField,who);
            }
        }
        return response;
    }

}
