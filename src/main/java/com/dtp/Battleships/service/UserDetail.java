package com.dtp.Battleships.service;

import com.dtp.Battleships.model.User;
import com.dtp.Battleships.model.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserDetail implements UserDetailsService {
    @Autowired
    private UserService userService;

    /**
     * Loads user details by username.
     *
     * @param  username  the username to load details for
     * @return           the user details loaded by username
     */
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userService.getUserByUsername(username);

        String userRole = null;

        for(Role role: user.getRoles()){
            userRole = role.getName().replace("ROLE_","");
        }


        return org.springframework.security.core.userdetails.User.builder()
                .username(user.getUsername())
                .password(user.getPassword())
                .roles(userRole) // Define the user's roles/authorities here
                .build();

    }
}
