package com.dtp.Battleships.service.indexservice;

public class Index {
    private int row;
    private int col;


    // Constructors:
    public Index(int row, int col) {
        this.row = row;
        this.col = col;
    }

    // Setter & Getter:
    public int getRow() {
        return row;
    }
    public int getCol() {
        return col;
    }
    public void setRow(int row) {
        this.row = row;
    }
    public void setCol(int col) {
        this.col = col;
    }
}
