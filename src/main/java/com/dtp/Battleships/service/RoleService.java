package com.dtp.Battleships.service;

import com.dtp.Battleships.model.Role;
import com.dtp.Battleships.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class RoleService {
    @Autowired
    private RoleRepository roleRepository;


    /**
     * Get a role by name.
     *
     * @param  name  the name of the role
     * @return       the role with the given name, or null if not found
     */
    public Role getRoleByName(String name){

        Optional<Role> roleOptional = roleRepository.findByName(name);

        if(roleOptional.isPresent()){
            return roleOptional.get();
        }

        return null;
    }

}
