package com.dtp.Battleships.service;

import com.dtp.Battleships.model.Scoreboard;
import com.dtp.Battleships.repository.ScoreboardRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ScoreboardService {
    @Autowired
    private ScoreboardRepository scoreboardRepository;

    /**
     * Saves the game to the scoreboard.
     *
     * @param  scoreboard  the scoreboard to save
     * @return             nothing
     */
    public void saveGameToScoreboard(Scoreboard scoreboard){

        scoreboardRepository.save(scoreboard);

    }

    /**
     * Retrieves the scoreboard for a specific game.
     *
     * @param  gameId   the ID of the game
     * @return         a list of Scoreboard objects for the specified game
     */
    public List<Scoreboard> getScoreboardForGame(int gameId){

        return scoreboardRepository.findAllByGameIdOrderByScoreAsc(gameId);

    }

    /**
     * Retrieves the complete (global) scoreboard.
     *
     * @return         	an iterable collection of Scoreboard objects
     */
    public Iterable<Scoreboard> getScoreboardComplete(){

        return scoreboardRepository.findAll();

    }

}
