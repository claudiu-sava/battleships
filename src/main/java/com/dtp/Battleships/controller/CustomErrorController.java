package com.dtp.Battleships.controller;

import com.dtp.Battleships.service.SessionService;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class CustomErrorController implements org.springframework.boot.web.servlet.error.ErrorController {

    @Autowired
    private SessionService sessionService;

    /**
     * A method to handle errors and redirects based on the status code.
     *
     * @param  request     the HTTP servlet request
     * @param  model       the model
     * @return             the error string or a redirect URL
     */
    @GetMapping("/error")
    public String error(HttpServletRequest request,
                        Model model) {

        Object status = request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE);

        model.addAttribute("loggedUser", sessionService.getLoggedUsername());

        if (status != null) {
            int statusCode = Integer.parseInt(status.toString());

            if(statusCode == HttpStatus.NOT_FOUND.value()) {
                return "redirect:/error?error404";
            }
            else if(statusCode == HttpStatus.INTERNAL_SERVER_ERROR.value()) {
                return "redirect:/error?error500";
            } else if(statusCode == HttpStatus.FORBIDDEN.value()){
                return "redirect:/error?forbidden";
            }
        }
        return "error";

    }
}

