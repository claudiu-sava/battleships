package com.dtp.Battleships.controller;

import com.dtp.Battleships.model.Game;
import com.dtp.Battleships.model.Scoreboard;
import com.dtp.Battleships.service.GameService;
import com.dtp.Battleships.service.RandomService;
import com.dtp.Battleships.service.ScoreboardService;
import com.dtp.Battleships.service.SessionService;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.*;
@Controller
@RequestMapping("/game")
public class GameController {

    @Autowired
    private GameService gameService;

    @Autowired
    private SessionService sessionService;

    @Autowired
    private ScoreboardService scoreboardService;

    @Autowired
    private RandomService randomService;

    @Value("${server.servlet.context-path}")
    private String contextPath;

    /**
     * Retrieves the game details for the specified game ID.
     *
     * @param  gameId    the ID of the game to retrieve
     * @param  model     the model object to add attributes to
     * @param  response  the HTTP servlet response object
     * @return           the name of the view to render
     */
    @GetMapping("/{gameId}")
    public String game(@PathVariable("gameId") int gameId,
                       Model model,
                       HttpServletResponse response){

        Game game = gameService.getGameById(gameId);
        if(game == null){
            return "redirect:/";
        }

        gameService.cleanGame(game);

        if(contextPath == null){
            contextPath = "/";
        }

        Cookie contextPathCookie = new Cookie("contextPath", contextPath);
        contextPathCookie.setPath(contextPath);
        response.addCookie(contextPathCookie);

        Cookie gameIdCookie = new Cookie("gameId", String.valueOf(gameId));
        gameIdCookie.setPath(contextPath);
        response.addCookie(gameIdCookie);

        Cookie difficultyCookie = new Cookie("difficulty", game.getDifficulty());
        difficultyCookie.setPath(contextPath);
        response.addCookie(difficultyCookie);

        Cookie fieldSizeCookie = new Cookie("field-size", String.valueOf(game.getFieldSize()));
        fieldSizeCookie.setPath(contextPath);
        response.addCookie(fieldSizeCookie);

        model.addAttribute("loggedUser", sessionService.getLoggedUsername());
        model.addAttribute("title", "BattleShips");
        model.addAttribute("playerField", gameService.fieldStringToArray(game.getPlayerField()));
        model.addAttribute("difficulty", game.getDifficulty());
        model.addAttribute("fieldSize", game.getFieldSize());
        model.addAttribute("gameId", gameId);
        return "startGame";
    }

    /**
     * Retrieves the play page for the BattleShips game.
     *
     * @param  model     the model used to pass data to the view
     * @param  gameId    the ID of the game retrieved from the cookie (default value is 0)
     * @param  response  the HTTP response object used to add cookies
     * @return           the name of the view to be rendered (play.html)
     */
    @GetMapping("/play")
    public String play(Model model,
                       @CookieValue(value = "gameId", defaultValue = "0") int gameId,
                       HttpServletResponse response){


        if(gameId == 0){
            return "redirect:/";
        }

        Game game = gameService.getGameById(gameId);
        if(game == null){
            return "redirect:/";
        }

        gameService.cleanGame(game);

        model.addAttribute("loggedUser", sessionService.getLoggedUsername());
        model.addAttribute("fieldSize", game.getFieldSize());
        model.addAttribute("playerField", gameService.fieldStringToArray(game.getPlayerField()));
        model.addAttribute("enemyField", gameService.fieldStringToArray(game.getEnemyField()));
        model.addAttribute("title", "BattleShips");
        model.addAttribute("playerHits", 0);
        model.addAttribute("enemyHits", 0);
        randomService.resetSmartRandomGeneration();
        return "play";
    }

    /**
     * Place ship on the user field.
     *
     * @param  gameId    the ID of the game
     * @param  cellList  list of cells to place the ship
     * @return           a hashmap containing the status of the ship placement
     */
    @PostMapping("/placeShip")
    @ResponseBody
    public HashMap<String, Integer> placeShipOnUserField(@RequestParam("gameId") int gameId,
                                                         @RequestBody List<String> cellList){

        HashMap<String, Integer> response = new HashMap<>();

        Game game = gameService.getGameById(gameId);

        String playerFieldString = game.getPlayerField();
        String[][] playerField = gameService.fieldStringToArray(playerFieldString);

        boolean error = false;
        boolean shipDeleted = false;

        for(String cell: cellList){

            int shipY = Integer.parseInt(cell.split("_")[1]);
            int shipX = Integer.parseInt(cell.split("_")[2]);

            int out = gameService.placeShip("x" ,shipY, shipX, playerField);

            if(out == 0){
                error = true;
                break;
            } else if (out == 2) {
                shipDeleted = true;
            }

        }

        game.setPlayerField(Arrays.deepToString(playerField));
        gameService.saveGame(game);

        if (error){
            response.put("status", 0);
        } else if (shipDeleted){
            response.put("status", 2);
        } else {
            response.put("status", 1);
        }

        return response;
    }

    /**
     * Shoots at the enemy's field and returns the result.
     *
     * @param  shipY    the Y coordinate of the ship
     * @param  shipX    the X coordinate of the ship
     * @param  gameId   the ID of the game
     * @return         the result of the shoot
     */
    @PostMapping("/shoot")
    @ResponseBody
    public HashMap<String, Integer> shoot(@RequestParam("shipY") int shipY,
                             @RequestParam("shipX") int shipX,
                             @RequestParam("gameId") int gameId){


        String[][] enemyField = gameService.fieldStringToArray(gameService.getGameById(gameId).getEnemyField());

        return gameService.shoot(shipY, shipX, enemyField);

    }

    /**
     * Function used by the AI to shoot back and return the result.
     *
     * @param  difficulty	description of parameter
     * @param  gameId	    description of parameter
     * @return         	description of return value
     */
    @PostMapping("/shootBack")
    @ResponseBody
    public HashMap<String, Integer> shootBack(@CookieValue(value = "difficulty", defaultValue = "easy") String difficulty,
                                              @RequestParam("gameId") int gameId){

        if(difficulty.equals("hacker")){
            return gameService.shootBackHacker(gameId);
        } else if(difficulty.equals("hard")){
            HashMap<String, Integer> shootBackSmartAnswer = gameService.shootBack(gameId,"Smart");
            return shootBackSmartAnswer;
        } else if(difficulty.equals("normal")){

            HashMap<String, Integer> shootBackNormalAnswer = gameService.shootBack(gameId,"Normal");


            return shootBackNormalAnswer;
        } else if(difficulty.equals("easy")){
            HashMap<String, Integer> shootBackDumbAnswer = gameService.shootBack(gameId,"Easy");
            return shootBackDumbAnswer;
        } else {
            return null;
        }


    }

    /**
     * Retrieves the player ratio for a given game ID.
     *
     * @param  gameId   the ID of the game
     * @return         the ratio as a HashMap
     */
    @GetMapping("/getRatio")
    @ResponseBody
    public HashMap<String, Double> getRatio(@RequestParam("gameId") int gameId){

        HashMap<String, Double> ratio = new HashMap<>();

        String[][] enemyField = gameService.fieldStringToArray(gameService.getGameById(gameId).getEnemyField());

        ratio.put("ratio", gameService.calculateRatio(enemyField));

        return ratio;

    }

    /**
     * Retrieves the enemy ratio for a given game ID.
     *
     * @param  gameId  the ID of the game
     * @return         a HashMap containing the enemy ratio
     */
    @GetMapping("/getRatioEnemy")
    @ResponseBody
    public HashMap<String, Double> getRatioEnemy(@RequestParam("gameId") int gameId){

        HashMap<String, Double> ratio = new HashMap<>();

        String[][] playerField = gameService.fieldStringToArray(gameService.getGameById(gameId).getPlayerField());

        ratio.put("ratio", gameService.calculateRatio(playerField));

        return ratio;

    }

    /**
     * Handles the "/gameOver" endpoint and processes the game over logic.
     *
     * @param  winner   the winner of the game
     * @param  score    the final score of the game
     * @param  gameId   the ID of the game
     * @param  model    the model for the view
     * @return          the view name for the game over page
     */
    @GetMapping("/gameOver")
    public String gameOver(@RequestParam("winner") String winner,
                           @RequestParam("score") String score,
                           @CookieValue(value = "gameId", defaultValue = "0") int gameId,
                           Model model){

        model.addAttribute("title", "Game Over");
        model.addAttribute("score", score);
        model.addAttribute("loggedUser", sessionService.getLoggedUsername());


        if(sessionService.getLoggedUsername().equals("anonymousUser")) {
            model.addAttribute("winner", winner);
        } else {
            model.addAttribute("winner", sessionService.getLoggedUsername());
        }


        Scoreboard gameScoreboard = new Scoreboard();

        Game game = gameService.getGameById(gameId);
        gameScoreboard.setUsername(sessionService.getLoggedUsername());
        gameScoreboard.setDifficulty(game.getDifficulty());
        gameScoreboard.setFieldSize(game.getFieldSize());
        gameScoreboard.setDifficulty(game.getDifficulty());
        gameScoreboard.setScore(Integer.parseInt(score));
        gameScoreboard.setGameId(gameId);

        scoreboardService.saveGameToScoreboard(gameScoreboard);

        model.addAttribute("games", scoreboardService.getScoreboardForGame(gameId));

        randomService.resetSmartRandomGeneration();
        return "gameOver";
    }

    /**
     * Reset the player's game field.
     *
     * @param  gameId   the ID of the game to reset
     * @return          a map containing the status of the reset operation
     */
    @PostMapping("/resetPlayerField")
    @ResponseBody
    public HashMap<String, Integer> resetGame(@CookieValue(value = "gameId", defaultValue = "-1") int gameId){

        HashMap<String, Integer> response = new HashMap<>();

        Game game = gameService.getGameById(gameId);

        if(gameId != -1){
            gameService.resetPlayerField(game);
            response.put("status", 1);
            return response;

        }

        response.put("status", 0);
        return response;

    }
}
