package com.dtp.Battleships.controller;

import com.dtp.Battleships.model.Game;
import com.dtp.Battleships.service.GameService;
import com.dtp.Battleships.service.ShipPlacementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.Arrays;

@Controller
public class IndexController {

    @Autowired
    private ShipPlacementService shipPlacementService;
    @Autowired
    private GameService gameService;


    /**
     * Retrieves the index page for the game.
     *
     * @param  fieldSize  the size of the game field (default is 13)
     * @param  difficulty the difficulty level of the game (default is normal)
     * @return            a string representing the redirect URL
     */
    @GetMapping("/")
    public String index(@CookieValue(value = "field-size", defaultValue = "13") String fieldSize,
                        @CookieValue(value = "difficulty", defaultValue = "normal") String difficulty) {

        int checkedFieldSize = gameService.checkFieldSize(Integer.parseInt(fieldSize));

        String[][] enemyField = new String[checkedFieldSize][checkedFieldSize];

        // place the ships on the field
        shipPlacementService.placeShip(5, enemyField);

        shipPlacementService.placeShip(4, enemyField);
        shipPlacementService.placeShip(4, enemyField);

        shipPlacementService.placeShip(3, enemyField);
        shipPlacementService.placeShip(3, enemyField);
        shipPlacementService.placeShip(3, enemyField);

        shipPlacementService.placeShip(2, enemyField);
        shipPlacementService.placeShip(2, enemyField);
        shipPlacementService.placeShip(2, enemyField);
        shipPlacementService.placeShip(2, enemyField);


        // save the game to the database
        Game game = new Game();
        game.setEnemyField(Arrays.deepToString(enemyField));
        game.setDifficulty(difficulty);
        game.setPlayerField(Arrays.deepToString(new String[checkedFieldSize][checkedFieldSize]));
        game.setFieldSize(checkedFieldSize);

        gameService.saveGame(game);

        int gameId = gameService.getGameId(game);

        return "redirect:/game/" + gameId;
    }


}
