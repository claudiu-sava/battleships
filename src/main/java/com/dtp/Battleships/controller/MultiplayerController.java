package com.dtp.Battleships.controller;

import com.dtp.Battleships.model.Game;
import com.dtp.Battleships.model.MultiplayerGame;
import com.dtp.Battleships.service.GameService;
import com.dtp.Battleships.service.MultiplayerGameService;
import com.dtp.Battleships.service.RandomService;
import com.dtp.Battleships.service.SessionService;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;

@Controller
@RequestMapping("/multiplayer")
public class MultiplayerController {

    @Autowired
    private RandomService randomService;
    @Autowired
    private GameService gameService;
    @Autowired
    private MultiplayerGameService multiplayerGameService;
    @Autowired
    private SessionService sessionService;

    @Value("${server.servlet.context-path}")
    private String contextPath;


    /**
     * Retrieves the normal game for field and sets up a multiplayer game (Entry point for Player 1).
     *
     * @param  model       the model to be used in the view
     * @param  gameId      the ID of the game
     * @param  response    the HTTP response
     * @return             the view for the multiplayer waiting lobby
     */
    @GetMapping("/game/{gameId}")
    public String multiplayer(Model model,
                              @PathVariable("gameId") int gameId,
                              HttpServletResponse response){


            //Retrieve the normal game for field
            Game game = gameService.getGameById(gameId);

            MultiplayerGame multiplayerGame = new MultiplayerGame();
            multiplayerGame.setId(randomService.generateRandom5DigitsPin());
            multiplayerGame.setPlayerOne(game.getPlayerField());
            multiplayerGame.setFieldSize(game.getFieldSize());
            multiplayerGame.setTurn(1);
            multiplayerGame.setPlayerOneHits(0);
            multiplayerGame.setPlayerTwoHits(0);
            multiplayerGame.setPlayerOneMisses(0);
            multiplayerGame.setPlayerTwoMisses(0);

            multiplayerGameService.saveMultiplayerGame(multiplayerGame);

            Cookie whoAmICookie = new Cookie("whoAmI", "p1");
            whoAmICookie.setPath(contextPath);
            response.addCookie(whoAmICookie);

            Cookie multiplayerGameIdCookie = new Cookie("multiplayerGameId", String.valueOf(multiplayerGame.getId()));
            multiplayerGameIdCookie.setPath(contextPath);
            response.addCookie(multiplayerGameIdCookie);

            model.addAttribute("gameId", multiplayerGame.getId());
            model.addAttribute("loggedUser", sessionService.getLoggedUsername());
            model.addAttribute("playerField", gameService.fieldStringToArray(multiplayerGame.getPlayerOne()));
            model.addAttribute("fieldSize", game.getFieldSize());
            model.addAttribute("title", "BattleShip :: Multiplayer");
            return "multiplayerWaitingLobby";

    }


    /**
     * Join a multiplayer game (Entry point for Player 2).
     *
     * @param  model		the model for the view
     * @param  multiplayerGameId	the id of the multiplayer game
     * @param  response		the HTTP response
     * @param  gameId		the id of the game
     * @return         		the view name for the multiplayer play
     */
    @GetMapping("/join/{multiplayerGameId}")
    // FOR PLAYER 2
    public String joinMultiplayer(Model model,
                                 @PathVariable("multiplayerGameId") int multiplayerGameId,
                                 HttpServletResponse response,
                                  @RequestParam("gameId") int gameId){

        Game game = gameService.getGameById(gameId);

        MultiplayerGame multiplayerGame = multiplayerGameService.getMultiplayerGameById(multiplayerGameId);

        if(multiplayerGame == null){
            return "redirect:/game/" + gameId + "?multiplayerGameNotFound";
        }

        multiplayerGame.setPlayerTwo(game.getPlayerField());

        if(game.getFieldSize() != multiplayerGame.getFieldSize()){
            return "redirect:/error?fieldSizeDismatch";
        }

        multiplayerGameService.saveMultiplayerGame(multiplayerGame);

        Cookie whoAmICookie = new Cookie("whoAmI", "p2");
        whoAmICookie.setPath(contextPath);
        response.addCookie(whoAmICookie);

        Cookie multiplayerGameIdCookie = new Cookie("multiplayerGameId", String.valueOf(multiplayerGame.getId()));
        multiplayerGameIdCookie.setPath(contextPath);
        response.addCookie(multiplayerGameIdCookie);

        gameService.cleanField(multiplayerGame.getPlayerTwo());
        gameService.cleanField(multiplayerGame.getPlayerOne());

        // Reset the scores
        multiplayerGame.setPlayerOneMisses(0);
        multiplayerGame.setPlayerTwoMisses(0);

        multiplayerGame.setPlayerOneHits(0);
        multiplayerGame.setPlayerTwoHits(0);

        model.addAttribute("multiplayerGameId", multiplayerGame.getId());
        model.addAttribute("loggedUser", sessionService.getLoggedUsername());
        model.addAttribute("playerField", gameService.fieldStringToArray(multiplayerGame.getPlayerTwo()));
        model.addAttribute("enemyField", gameService.fieldStringToArray(multiplayerGame.getPlayerOne()));
        model.addAttribute("fieldSize", game.getFieldSize());
        model.addAttribute("title", "BattleShip :: Multiplayer");

        return "multiplayerPlay";

    }

    /**
     * A method to handle playing multiplayer game.
     *
     * @param  model            the model to be used for the view
     * @param  multiplayerGameId  the ID of the multiplayer game
     * @return                  the view name for multiplayer play
     */
    @GetMapping("/play")
    public String playMultiplayer(Model model,
                                 @RequestParam("gameId") int multiplayerGameId){

        MultiplayerGame multiplayerGame = multiplayerGameService.getMultiplayerGameById(multiplayerGameId);

        String playerOne = gameService.cleanField(multiplayerGame.getPlayerOne());
        String playerTwo = gameService.cleanField(multiplayerGame.getPlayerTwo());

        multiplayerGame.setPlayerOne(playerOne);
        multiplayerGame.setPlayerTwo(playerTwo);

        // Reset the scores
        multiplayerGame.setPlayerOneMisses(0);
        multiplayerGame.setPlayerTwoMisses(0);

        multiplayerGame.setPlayerOneHits(0);
        multiplayerGame.setPlayerTwoHits(0);

        multiplayerGameService.saveMultiplayerGame(multiplayerGame);

        model.addAttribute("multiplayerGameId", multiplayerGame.getId());
        model.addAttribute("playerField", gameService.fieldStringToArray(playerOne));
        model.addAttribute("enemyField", gameService.fieldStringToArray(playerTwo));
        model.addAttribute("fieldSize", multiplayerGame.getFieldSize());
        model.addAttribute("loggedUser", sessionService.getLoggedUsername());
        model.addAttribute("title", "BattleShip :: Multiplayer");

        return "multiplayerPlay";
    }

    /**
     * Checks if the player 2 is ready for the game.
     *
     * @param  gameId  the ID of the game
     * @return        "yes" if the player 2 is ready, "no" otherwise
     */
    @PostMapping("/isEnemyReady")
    @ResponseBody
    public String isEnemyReady(@RequestParam("gameId") int gameId){

        MultiplayerGame multiplayerGame = multiplayerGameService.getMultiplayerGameById(gameId);

        if (multiplayerGame.getPlayerTwo() == null){
            return "no";
        }


        return "yes";
    }

    /**
     * Shoots at the specified coordinates in a multiplayer game and returns the result.
     *
     * @param  shipY         the Y coordinate of the target ship
     * @param  shipX         the X coordinate of the target ship
     * @param  multiplayerGameId   the ID of the multiplayer game
     * @param  whoAmI        the player's identity
     * @return               the result of the shooting action
     */
    @PostMapping("/shoot")
    @ResponseBody

    public HashMap<String, Integer> shoot(@RequestParam("shipY") int shipY,
                         @RequestParam("shipX") int shipX,
                         @RequestParam("gameId") int multiplayerGameId,
                         @CookieValue(value = "whoAmI", defaultValue = "null") String whoAmI){


        MultiplayerGame multiplayerGame = multiplayerGameService.getMultiplayerGameById(multiplayerGameId);

        HashMap<String, Integer> shootAnswer = multiplayerGameService.shoot(shipY, shipX, multiplayerGame, whoAmI);

        if(shootAnswer != null){

            return shootAnswer;

        }


        return null;
    }

    /**
     * Updates the multiplayer game and returns the player field.
     *
     * @param  multiplayerGameId   the ID of the multiplayer game
     * @param  model               the model to be updated
     * @param  whoAmI              the identity of the player
     * @return                     the player field
     */
    @GetMapping("/update")
    public String update(@RequestParam("gameId") int multiplayerGameId,
                                              Model model,
                         @CookieValue(value = "whoAmI", defaultValue = "null") String whoAmI){

        MultiplayerGame multiplayerGame = multiplayerGameService.getMultiplayerGameById(multiplayerGameId);

        if(whoAmI.equals("p1")){
            model.addAttribute("playerField", gameService.fieldStringToArray(multiplayerGame.getPlayerOne()));
            if(multiplayerGame.getTurn().equals(1)){
                model.addAttribute("turn", "true");
            } else {
                model.addAttribute("turn", "false");
            }
        } else {
            model.addAttribute("playerField", gameService.fieldStringToArray(multiplayerGame.getPlayerTwo()));
            if(multiplayerGame.getTurn().equals(2)){
                model.addAttribute("turn", "true");
            } else {
                model.addAttribute("turn", "false");
            }
        }


        model.addAttribute("fieldSize", multiplayerGame.getFieldSize());
        return "/fragments/playerField";
    }

    /**
     * Update the score table for the multiplayer game based on the provided game ID and player's information.
     *
     * @param  multiplayerGameId  the ID of the multiplayer game
     * @param  model              the model to be updated with the score table information
     * @param  whoAmI             the identity of the player
     * @return                   the path to the score table fragment
     */
    @GetMapping("/updateScoreTable")
    public String updateScoreTable(@RequestParam("gameId") int multiplayerGameId,
                                   Model model,
                                   @CookieValue(value = "whoAmI", defaultValue = "null") String whoAmI){

        MultiplayerGame multiplayerGame = multiplayerGameService.getMultiplayerGameById(multiplayerGameId);

        String[][] playerOneField = gameService.fieldStringToArray(multiplayerGame.getPlayerOne());
        String[][] playerTwoField = gameService.fieldStringToArray(multiplayerGame.getPlayerTwo());

        if(whoAmI.equals("p1")){
            model.addAttribute("myHits", multiplayerGame.getPlayerOneHits());
            model.addAttribute("myMisses", multiplayerGame.getPlayerOneMisses());
            model.addAttribute("myShoots", multiplayerGame.getPlayerOneMisses() + multiplayerGame.getPlayerOneHits());

            model.addAttribute("myScore", (multiplayerGame.getPlayerOneHits() - multiplayerGame.getPlayerTwoHits()) * 100);

            model.addAttribute("enemyLeft", String.format("%.2f", multiplayerGameService.calculateRatio(playerOneField)));
            model.addAttribute("myLeft", String.format("%.2f", multiplayerGameService.calculateRatio(playerTwoField)));

            model.addAttribute("enemyHits", multiplayerGame.getPlayerTwoHits());
            model.addAttribute("enemyMisses", multiplayerGame.getPlayerTwoMisses());
            model.addAttribute("enemyShoots", multiplayerGame.getPlayerTwoMisses() + multiplayerGame.getPlayerTwoHits());
        } else {
            model.addAttribute("myHits", multiplayerGame.getPlayerTwoHits());
            model.addAttribute("myMisses", multiplayerGame.getPlayerTwoMisses());
            model.addAttribute("myShoots", multiplayerGame.getPlayerTwoMisses() + multiplayerGame.getPlayerTwoHits());

            model.addAttribute("myScore", (multiplayerGame.getPlayerTwoHits() - multiplayerGame.getPlayerOneHits()) * 100);

            model.addAttribute("enemyLeft", String.format("%.2f", multiplayerGameService.calculateRatio(playerTwoField)));
            model.addAttribute("myLeft", String.format("%.2f", multiplayerGameService.calculateRatio(playerOneField)));

            model.addAttribute("enemyHits", multiplayerGame.getPlayerOneHits());
            model.addAttribute("enemyMisses", multiplayerGame.getPlayerOneMisses());
            model.addAttribute("enemyShoots", multiplayerGame.getPlayerOneMisses() + multiplayerGame.getPlayerOneHits());
        }
        return "/fragments/scoreTable";

    }

}
