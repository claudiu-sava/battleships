package com.dtp.Battleships.controller;

import com.dtp.Battleships.dto.UserLoginDto;
import com.dtp.Battleships.model.Role;
import com.dtp.Battleships.model.User;
import com.dtp.Battleships.service.RoleService;
import com.dtp.Battleships.service.SessionService;
import com.dtp.Battleships.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.Collections;

import static com.dtp.Battleships.security.SecurityConfiguration.passwordEncoder;

@Controller
public class LoginController {
    @Autowired
    private RoleService roleService;
    @Autowired
    private UserService userService;
    @Autowired
    private SessionService sessionService;

    @GetMapping("/sign-up")
    public String signUpPage(Model model){

        model.addAttribute("title", "Sign-up");
        model.addAttribute("loggedUser", sessionService.getLoggedUsername());
        return "sign-up";
    }

    /**
     * Process the user sign-up and redirect to the login page upon success.
     *
     * @param  userLoginDto   the user login data transfer object
     * @return                a redirect string to the login page upon success
     */
    @PostMapping("/sign-up")
    public String signUp(@ModelAttribute UserLoginDto userLoginDto){

        User userToCheckIfNameExists = userService.getUserByUsername(userLoginDto.getUsername());
        if(userToCheckIfNameExists != null){
            return "redirect:/sign-up?usernameExists";
        } else {

            User user = new User();
            user.setUsername(userLoginDto.getUsername());
            user.setPassword(passwordEncoder().encode(userLoginDto.getPassword()));

            Role role = roleService.getRoleByName("ROLE_USER");
            user.setRoles(Collections.singleton(role));

            userService.saveUser(user);

            return "redirect:/login?success";
        }
    }

    /**
     * A method to handle the GET request for the "/login" endpoint.
     *
     * @param  model  the model object
     * @return       the login view
     */
    @GetMapping("/login")
    public String loginPage(Model model){

        model.addAttribute("title", "Login");
        model.addAttribute("loggedUser", sessionService.getLoggedUsername());
        return "login";

    }

}
