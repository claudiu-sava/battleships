package com.dtp.Battleships.controller;

import com.dtp.Battleships.service.SessionService;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/settings")
public class SettingsController {

    @Autowired
    private SessionService sessionService;

    @Value("${server.servlet.context-path}")
    private String contextPath;

    /**
     * A description of the entire Java function.
     *
     * @param  model         an object of type Model
     * @param  fieldSize     a String representing the value of the "field-size" cookie (default value: "15")
     * @param  difficulty    a String representing the value of the "difficulty" cookie (default value: "easy")
     * @return               a String representing the name of the view to be rendered ("settings")
     */
    @GetMapping("/")
    public String settingsPage(Model model,
                               @CookieValue(value = "field-size", defaultValue = "15") String fieldSize,
                               @CookieValue(value = "difficulty", defaultValue = "easy") String difficulty){


        model.addAttribute("fieldSize", fieldSize);
        model.addAttribute("difficulty", difficulty);
        model.addAttribute("title", "Settings");
        model.addAttribute("loggedUser", sessionService.getLoggedUsername());
        return "settings";

    }


    /**
     * Sets the difficulty level for the game.
     *
     * @param  difficulty   the difficulty level to be set
     * @param  response     the HttpServletResponse object
     * @return              a String for redirecting to the settings page
     */
    @PostMapping("/difficulty")
    public String setDifficulty(@RequestParam("difficulty") String difficulty,
                                HttpServletResponse response){

        Cookie cookie = new Cookie("difficulty", difficulty);
        cookie.setPath(contextPath);
        response.addCookie(cookie);
        return "redirect:/settings/";
    }

    /**
     * Set the field size and create a cookie.
     *
     * @param  fieldSize   the size of the field
     * @param  response    the HttpServletResponse
     * @return             a redirect to the settings page
     */
    @PostMapping("/setFieldSize")
    public String setFieldSize(@RequestParam("field-size") String fieldSize,
                               HttpServletResponse response){

        Cookie cookie = new Cookie("field-size", fieldSize);
        cookie.setPath(contextPath);
        response.addCookie(cookie);
        return "redirect:/settings/";
    }

}
