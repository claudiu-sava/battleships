package com.dtp.Battleships.controller;

import com.dtp.Battleships.service.ScoreboardService;
import com.dtp.Battleships.service.SessionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ScoreboardController {
    @Autowired
    private ScoreboardService scoreboardService;
    @Autowired
    private SessionService sessionService;


    /**
     * Retrieves the scoreboard information and populates the model with the title,
     * games, and logged user information.
     *
     * @param  model  the model to populate with scoreboard information
     * @return       the name of the template to render
     */
    @GetMapping("/scoreboard")
    public String getScoreboard(Model model){

        model.addAttribute("title", "Scoreboard");
        model.addAttribute("games", scoreboardService.getScoreboardComplete());
        model.addAttribute("loggedUser", sessionService.getLoggedUsername());

        return "scoreboard";
    }

}
