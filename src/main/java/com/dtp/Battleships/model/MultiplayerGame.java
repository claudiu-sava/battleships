package com.dtp.Battleships.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Setter
@Entity
@Getter
public class MultiplayerGame {

    @Id
    private Integer id;


    @Column(length = 4000)
    private String playerOne;


    @Column(length = 4000)
    private String playerTwo;


    private Integer fieldSize;

    // 1 = player one's turn, 2 = player two's turn
    private Integer turn;

    private Integer playerOneHits, playerTwoHits, playerOneMisses, playerTwoMisses;


}
