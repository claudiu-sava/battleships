package com.dtp.Battleships.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
public class Game {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Setter
    private Integer id;

    @Setter
    @Column(length = 4000)
    private String enemyField;

    @Setter
    private Integer fieldSize;

    @Setter
    @Column(length = 4000)
    private String playerField;

    @Setter
    private String difficulty;

}
