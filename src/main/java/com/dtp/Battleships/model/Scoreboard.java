package com.dtp.Battleships.model;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
public class Scoreboard {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Setter
    private String username;

    @Setter
    private Integer score;

    @Setter
    private String difficulty;

    @Setter
    private Integer fieldSize;

    @Setter
    private Integer gameId;

}
