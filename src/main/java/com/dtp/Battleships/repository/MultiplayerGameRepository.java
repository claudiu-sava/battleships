package com.dtp.Battleships.repository;

import com.dtp.Battleships.model.MultiplayerGame;
import org.springframework.data.repository.CrudRepository;

public interface MultiplayerGameRepository extends CrudRepository<MultiplayerGame, Integer> {



}
