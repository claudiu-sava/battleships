package com.dtp.Battleships.repository;

import com.dtp.Battleships.model.Game;
import org.springframework.data.repository.CrudRepository;

public interface GameRepository extends CrudRepository<Game, Integer> {



}
