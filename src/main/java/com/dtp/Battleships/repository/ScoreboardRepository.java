package com.dtp.Battleships.repository;

import com.dtp.Battleships.model.Scoreboard;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ScoreboardRepository extends CrudRepository<Scoreboard, Integer> {

    List<Scoreboard> findAllByGameIdOrderByScoreAsc(Integer gameId);

}
