package com.dtp.Battleships.service;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.BDDMockito.given;

@ExtendWith(SpringExtension.class)
class ShipPlacementServiceTest {

    @InjectMocks
    ShipPlacementService underTest;

    @Mock
    RandomService randomService;

    @Test
    void checkRight_PossibleWithBorderDown() {

        // GIVEN
        String[][] field = new String[2][2];
        int shipSize = 2;
        int startingY = 0;
        int startingX = 0;

        // WHEN
        underTest.checkRight(shipSize, field, startingY, startingX);

        // THEN
        assertEquals(true, underTest.checkRight(shipSize, field, startingY, startingX).get("canBePlaced"));
        assertEquals(false, underTest.checkRight(shipSize, field, startingY, startingX).get("borderUp"));
        assertEquals(true, underTest.checkRight(shipSize, field, startingY, startingX).get("borderDown"));
        assertEquals(false, underTest.checkRight(shipSize, field, startingY, startingX).get("borderLeft"));
        assertEquals(false, underTest.checkRight(shipSize, field, startingY, startingX).get("borderRight"));

    }

    @Test
    void checkRight_PossibleWithBorderUp() {

        // GIVEN
        String[][] field = new String[2][2];
        int shipSize = 2;
        int startingY = 1;
        int startingX = 0;

        // WHEN
        underTest.checkRight(shipSize, field, startingY, startingX);

        // THEN
        assertEquals(true, underTest.checkRight(shipSize, field, startingY, startingX).get("canBePlaced"));
        assertEquals(true, underTest.checkRight(shipSize, field, startingY, startingX).get("borderUp"));
        assertEquals(false, underTest.checkRight(shipSize, field, startingY, startingX).get("borderDown"));
        assertEquals(false, underTest.checkRight(shipSize, field, startingY, startingX).get("borderLeft"));
        assertEquals(false, underTest.checkRight(shipSize, field, startingY, startingX).get("borderRight"));

    }

    @Test
    void checkRight_PossibleWithBorderRight() {

        // GIVEN
        String[][] field = new String[3][3];
        int shipSize = 2;
        int startingY = 1;
        int startingX = 0;

        // WHEN
        underTest.checkRight(shipSize, field, startingY, startingX);

        // THEN
        assertEquals(true, underTest.checkRight(shipSize, field, startingY, startingX).get("canBePlaced"));
        assertEquals(true, underTest.checkRight(shipSize, field, startingY, startingX).get("borderUp"));
        assertEquals(true, underTest.checkRight(shipSize, field, startingY, startingX).get("borderDown"));
        assertEquals(false, underTest.checkRight(shipSize, field, startingY, startingX).get("borderLeft"));
        assertEquals(true, underTest.checkRight(shipSize, field, startingY, startingX).get("borderRight"));

    }

    @Test
    void checkRight_PossibleWithBorderLeft() {

        // GIVEN
        String[][] field = new String[3][3];
        int shipSize = 2;
        int startingY = 1;
        int startingX = 1;

        // WHEN
        underTest.checkRight(shipSize, field, startingY, startingX);

        // THEN
        assertEquals(true, underTest.checkRight(shipSize, field, startingY, startingX).get("canBePlaced"));
        assertEquals(true, underTest.checkRight(shipSize, field, startingY, startingX).get("borderUp"));
        assertEquals(true, underTest.checkRight(shipSize, field, startingY, startingX).get("borderDown"));
        assertEquals(true, underTest.checkRight(shipSize, field, startingY, startingX).get("borderLeft"));
        assertEquals(false, underTest.checkRight(shipSize, field, startingY, startingX).get("borderRight"));

    }

    @Test
    void checkRight_PossibleWithAllBorders() {

        // GIVEN
        String[][] field = new String[10][10];
        int shipSize = 2;
        int startingY = 4;
        int startingX = 4;

        // WHEN
        underTest.checkRight(shipSize, field, startingY, startingX);

        // THEN
        assertEquals(true, underTest.checkRight(shipSize, field, startingY, startingX).get("canBePlaced"));
        assertEquals(true, underTest.checkRight(shipSize, field, startingY, startingX).get("borderUp"));
        assertEquals(true, underTest.checkRight(shipSize, field, startingY, startingX).get("borderDown"));
        assertEquals(true, underTest.checkRight(shipSize, field, startingY, startingX).get("borderLeft"));
        assertEquals(true, underTest.checkRight(shipSize, field, startingY, startingX).get("borderRight"));

    }
    @Test
    void checkRight_NotPossible() {

        // GIVEN
        String[][] field = new String[2][2];
        int shipSize = 2;
        int startingY = 0;
        int startingX = 1;

        // WHEN
        underTest.checkRight(shipSize, field, startingY, startingX);

        // THEN
        assertEquals(false, underTest.checkRight(shipSize, field, startingY, startingX).get("canBePlaced"));

    }

    @Test
    void checkLeft_PossibleWithBorderDown() {

        //GIVEN
        String[][] field = new String[2][2];
        int shipSize = 2;
        int startingY = 0;
        int startingX = 1;

        //WHEN
        underTest.checkLeft(shipSize, field, startingY, startingX);

        //THEN
        assertEquals(true, underTest.checkLeft(shipSize, field, startingY, startingX).get("canBePlaced"));
        assertEquals(false, underTest.checkLeft(shipSize, field, startingY, startingX).get("borderUp"));
        assertEquals(true, underTest.checkLeft(shipSize, field, startingY, startingX).get("borderDown"));
        assertEquals(false, underTest.checkLeft(shipSize, field, startingY, startingX).get("borderLeft"));
        assertEquals(false, underTest.checkLeft(shipSize, field, startingY, startingX).get("borderRight"));

    }

    @Test
    void checkLeft_PossibleWithBorderUp() {

        //GIVEN
        String[][] field = new String[2][2];
        int shipSize = 2;
        int startingY = 1;
        int startingX = 1;

        //WHEN
        underTest.checkLeft(shipSize, field, startingY, startingX);

        //THEN
        assertEquals(true, underTest.checkLeft(shipSize, field, startingY, startingX).get("canBePlaced"));
        assertEquals(true, underTest.checkLeft(shipSize, field, startingY, startingX).get("borderUp"));
        assertEquals(false, underTest.checkLeft(shipSize, field, startingY, startingX).get("borderDown"));
        assertEquals(false, underTest.checkLeft(shipSize, field, startingY, startingX).get("borderLeft"));
        assertEquals(false, underTest.checkLeft(shipSize, field, startingY, startingX).get("borderRight"));

    }

    @Test
    void checkLeft_PossibleWithBorderRight() {

        // GIVEN
        String[][] field = new String[3][3];
        int shipSize = 2;
        int startingY = 1;
        int startingX = 1;

        // WHEN
        underTest.checkLeft(shipSize, field, startingY, startingX);

        // THEN
        assertEquals(true, underTest.checkLeft(shipSize, field, startingY, startingX).get("canBePlaced"));
        assertEquals(true, underTest.checkLeft(shipSize, field, startingY, startingX).get("borderUp"));
        assertEquals(true, underTest.checkLeft(shipSize, field, startingY, startingX).get("borderDown"));
        assertEquals(false, underTest.checkLeft(shipSize, field, startingY, startingX).get("borderLeft"));
        assertEquals(true, underTest.checkLeft(shipSize, field, startingY, startingX).get("borderRight"));

    }

    @Test
    void checkLeft_PossibleWithBorderLeft() {

        // GIVEN
        String[][] field = new String[3][3];
        int shipSize = 2;
        int startingY = 1;
        int startingX = 2;

        // WHEN
        underTest.checkLeft(shipSize, field, startingY, startingX);

        // THEN
        assertEquals(true, underTest.checkLeft(shipSize, field, startingY, startingX).get("canBePlaced"));
        assertEquals(true, underTest.checkLeft(shipSize, field, startingY, startingX).get("borderUp"));
        assertEquals(true, underTest.checkLeft(shipSize, field, startingY, startingX).get("borderDown"));
        assertEquals(true, underTest.checkLeft(shipSize, field, startingY, startingX).get("borderLeft"));
        assertEquals(false, underTest.checkLeft(shipSize, field, startingY, startingX).get("borderRight"));

    }

    @Test
    void checkLeft_PossibleWithAllBorders() {

        // GIVEN
        String[][] field = new String[10][10];
        int shipSize = 2;
        int startingY = 4;
        int startingX = 4;

        // WHEN
        underTest.checkLeft(shipSize, field, startingY, startingX);

        // THEN
        assertEquals(true, underTest.checkLeft(shipSize, field, startingY, startingX).get("canBePlaced"));
        assertEquals(true, underTest.checkLeft(shipSize, field, startingY, startingX).get("borderUp"));
        assertEquals(true, underTest.checkLeft(shipSize, field, startingY, startingX).get("borderDown"));
        assertEquals(true, underTest.checkLeft(shipSize, field, startingY, startingX).get("borderLeft"));
        assertEquals(true, underTest.checkLeft(shipSize, field, startingY, startingX).get("borderRight"));

    }

    @Test
    void checkLeft_NotPossible() {

        //GIVEN
        String[][] field = new String[2][2];
        int shipSize = 2;
        int startingY = 1;
        int startingX = 0;

        //WHEN
        underTest.checkLeft(shipSize, field, startingY, startingX);

        //THEN
        assertEquals(false, underTest.checkLeft(shipSize, field, startingY, startingX).get("canBePlaced"));

    }

    @Test
    void checkUp_PossibleWithBorderRight() {

        // GIVEN
        String[][] field = new String[2][2];
        int shipSize = 2;
        int startingY = 1;
        int startingX = 0;

        // WHEN
        underTest.checkUp(shipSize, field, startingY, startingX);

        // THEN
        assertEquals(true, underTest.checkUp(shipSize, field, startingY, startingX).get("canBePlaced"));
        assertEquals(false, underTest.checkUp(shipSize, field, startingY, startingX).get("borderUp"));
        assertEquals(false, underTest.checkUp(shipSize, field, startingY, startingX).get("borderDown"));
        assertEquals(false, underTest.checkUp(shipSize, field, startingY, startingX).get("borderLeft"));
        assertEquals(true, underTest.checkUp(shipSize, field, startingY, startingX).get("borderRight"));

    }

    @Test
    void checkUp_PossibleWithBorderLeft() {

        // GIVEN
        String[][] field = new String[2][2];
        int shipSize = 2;
        int startingY = 1;
        int startingX = 1;

        // WHEN
        underTest.checkUp(shipSize, field, startingY, startingX);

        // THEN
        assertEquals(true, underTest.checkUp(shipSize, field, startingY, startingX).get("canBePlaced"));
        assertEquals(false, underTest.checkUp(shipSize, field, startingY, startingX).get("borderUp"));
        assertEquals(false, underTest.checkUp(shipSize, field, startingY, startingX).get("borderDown"));
        assertEquals(true, underTest.checkUp(shipSize, field, startingY, startingX).get("borderLeft"));
        assertEquals(false, underTest.checkUp(shipSize, field, startingY, startingX).get("borderRight"));

    }

    @Test
    void checkUp_PossibleWithBorderUp() {

        // GIVEN
        String[][] field = new String[3][3];
        int shipSize = 2;
        int startingY = 2;
        int startingX = 0;

        // WHEN
        underTest.checkUp(shipSize, field, startingY, startingX);

        // THEN
        assertEquals(true, underTest.checkUp(shipSize, field, startingY, startingX).get("canBePlaced"));
        assertEquals(true, underTest.checkUp(shipSize, field, startingY, startingX).get("borderUp"));
        assertEquals(false, underTest.checkUp(shipSize, field, startingY, startingX).get("borderDown"));
        assertEquals(false, underTest.checkUp(shipSize, field, startingY, startingX).get("borderLeft"));
        assertEquals(true, underTest.checkUp(shipSize, field, startingY, startingX).get("borderRight"));

    }

    @Test
    void checkUp_PossibleWithBorderDown() {

        // GIVEN
        String[][] field = new String[3][3];
        int shipSize = 2;
        int startingY = 1;
        int startingX = 0;

        // WHEN
        underTest.checkUp(shipSize, field, startingY, startingX);

        // THEN
        assertEquals(true, underTest.checkUp(shipSize, field, startingY, startingX).get("canBePlaced"));
        assertEquals(false, underTest.checkUp(shipSize, field, startingY, startingX).get("borderUp"));
        assertEquals(true, underTest.checkUp(shipSize, field, startingY, startingX).get("borderDown"));
        assertEquals(false, underTest.checkUp(shipSize, field, startingY, startingX).get("borderLeft"));
        assertEquals(true, underTest.checkUp(shipSize, field, startingY, startingX).get("borderRight"));

    }

    @Test
    void checkUp_PossibleWithAllBorders() {

        // GIVEN
        String[][] field = new String[10][10];
        int shipSize = 2;
        int startingY = 4;
        int startingX = 4;

        // WHEN
        underTest.checkUp(shipSize, field, startingY, startingX);

        // THEN
        assertEquals(true, underTest.checkUp(shipSize, field, startingY, startingX).get("canBePlaced"));
        assertEquals(true, underTest.checkUp(shipSize, field, startingY, startingX).get("borderUp"));
        assertEquals(true, underTest.checkUp(shipSize, field, startingY, startingX).get("borderDown"));
        assertEquals(true, underTest.checkUp(shipSize, field, startingY, startingX).get("borderLeft"));
        assertEquals(true, underTest.checkUp(shipSize, field, startingY, startingX).get("borderRight"));

    }

    @Test
    void checkUp_NotPossible() {

        // GIVEN
        String[][] field = new String[2][2];
        int shipSize = 2;
        int startingY = 0;
        int startingX = 0;

        // WHEN
        underTest.checkUp(shipSize, field, startingY, startingX);

        // THEN
        assertEquals(false, underTest.checkUp(shipSize, field, startingY, startingX).get("canBePlaced"));

    }

    @Test
    void checkDown_PossibleWithBorderRight() {

        //GIVEN
        String[][] field = new String[2][2];
        int shipSize = 2;
        int startingY = 0;
        int startingX = 0;

        // WHEN
        underTest.checkDown(shipSize, field, startingY, startingX);

        // THEN
        assertEquals(true, underTest.checkDown(shipSize, field, startingY, startingX).get("canBePlaced"));
        assertEquals(false, underTest.checkDown(shipSize, field, startingY, startingX).get("borderUp"));
        assertEquals(false, underTest.checkDown(shipSize, field, startingY, startingX).get("borderDown"));
        assertEquals(false, underTest.checkDown(shipSize, field, startingY, startingX).get("borderLeft"));
        assertEquals(true, underTest.checkDown(shipSize, field, startingY, startingX).get("borderRight"));

    }

    @Test
    void checkDown_PossibleWithBorderLeft() {

        //GIVEN
        String[][] field = new String[2][2];
        int shipSize = 2;
        int startingY = 0;
        int startingX = 1;

        // WHEN
        underTest.checkDown(shipSize, field, startingY, startingX);

        // THEN
        assertEquals(true, underTest.checkDown(shipSize, field, startingY, startingX).get("canBePlaced"));
        assertEquals(false, underTest.checkDown(shipSize, field, startingY, startingX).get("borderUp"));
        assertEquals(false, underTest.checkDown(shipSize, field, startingY, startingX).get("borderDown"));
        assertEquals(true, underTest.checkDown(shipSize, field, startingY, startingX).get("borderLeft"));
        assertEquals(false, underTest.checkDown(shipSize, field, startingY, startingX).get("borderRight"));

    }

    @Test
    void checkDown_PossibleWithBorderUp() {

        // GIVEN
        String[][] field = new String[3][3];
        int shipSize = 2;
        int startingY = 1;
        int startingX = 0;

        // WHEN
        underTest.checkDown(shipSize, field, startingY, startingX);

        // THEN
        assertEquals(true, underTest.checkDown(shipSize, field, startingY, startingX).get("canBePlaced"));
        assertEquals(true, underTest.checkDown(shipSize, field, startingY, startingX).get("borderUp"));
        assertEquals(false, underTest.checkDown(shipSize, field, startingY, startingX).get("borderDown"));
        assertEquals(false, underTest.checkDown(shipSize, field, startingY, startingX).get("borderLeft"));
        assertEquals(true, underTest.checkDown(shipSize, field, startingY, startingX).get("borderRight"));

    }

    @Test
    void checkDown_PossibleWithBorderDown() {

        // GIVEN
        String[][] field = new String[3][3];
        int shipSize = 2;
        int startingY = 0;
        int startingX = 0;

        // WHEN
        underTest.checkDown(shipSize, field, startingY, startingX);

        // THEN
        assertEquals(true, underTest.checkDown(shipSize, field, startingY, startingX).get("canBePlaced"));
        assertEquals(false, underTest.checkDown(shipSize, field, startingY, startingX).get("borderUp"));
        assertEquals(true, underTest.checkDown(shipSize, field, startingY, startingX).get("borderDown"));
        assertEquals(false, underTest.checkDown(shipSize, field, startingY, startingX).get("borderLeft"));
        assertEquals(true, underTest.checkDown(shipSize, field, startingY, startingX).get("borderRight"));

    }

    @Test
    void checkDown_PossibleWithAllBorders() {

        // GIVEN
        String[][] field = new String[10][10];
        int shipSize = 2;
        int startingY = 4;
        int startingX = 4;

        // WHEN
        underTest.checkDown(shipSize, field, startingY, startingX);

        // THEN
        assertEquals(true, underTest.checkDown(shipSize, field, startingY, startingX).get("canBePlaced"));
        assertEquals(true, underTest.checkDown(shipSize, field, startingY, startingX).get("borderUp"));
        assertEquals(true, underTest.checkDown(shipSize, field, startingY, startingX).get("borderDown"));
        assertEquals(true, underTest.checkDown(shipSize, field, startingY, startingX).get("borderLeft"));
        assertEquals(true, underTest.checkDown(shipSize, field, startingY, startingX).get("borderRight"));

    }

    @Test
    void checkDown_NotPossible() {

        //GIVEN
        String[][] field = new String[2][2];
        int shipSize = 2;
        int startingY = 1;
        int startingX = 0;

        // WHEN
        underTest.checkDown(shipSize, field, startingY, startingX);

        // THEN
        assertEquals(false, underTest.checkDown(shipSize, field, startingY, startingX).get("canBePlaced"));

    }

    @Test
    void placeShip_PlaceShipRight() {

        //GIVEN
        int shipSize = 2;
        String[][] field = new String[3][3];
        given(randomService.generateRandomNumber(field.length)).willReturn(0);

        // WHEN
        String[][] newField = underTest.placeShip(shipSize, field);

        //THEN
        assertEquals(newField[0][0], "x");
        assertEquals(newField[0][1], "x");
        assertEquals(newField[0][2], "b");
        assertEquals(newField[1][0], "b");
        assertEquals(newField[1][1], "b");

    }

    @Test
    void placeShip_PlaceShipLeft() {

        //GIVEN
        int shipSize = 2;
        String[][] field = new String[3][3];
        given(randomService.generateRandomNumber(field.length)).willReturn(2);

        // WHEN
        String[][] newField = underTest.placeShip(shipSize, field);

        //THEN
        assertEquals(newField[2][2], "x");
        assertEquals(newField[2][1], "x");
        assertEquals(newField[2][0], "b");
        assertEquals(newField[1][1], "b");
        assertEquals(newField[1][2], "b");

    }

    @Test
    void placeShip_PlaceShipUp() {

        //GIVEN
        int shipSize = 3;
        String[][] field = new String[4][4];
        field[3][1] = "x";
        given(randomService.generateRandomNumber(field.length)).willReturn(2);

        // WHEN
        String[][] newField = underTest.placeShip(shipSize, field);

        //THEN
        assertEquals(newField[2][2], "x");
        assertEquals(newField[1][2], "x");
        assertEquals(newField[0][2], "x");
        assertEquals(newField[2][1], "b");
        assertEquals(newField[1][1], "b");
        assertEquals(newField[0][1], "b");
        assertEquals(newField[2][3], "b");
        assertEquals(newField[1][3], "b");
        assertEquals(newField[0][3], "b");

    }

    @Test
    void placeShip_PlaceShipDown() {

        //GIVEN
        int shipSize = 3;
        String[][] field = new String[4][4];
        field[0][2] = "x";
        given(randomService.generateRandomNumber(field.length)).willReturn(1);

        // WHEN
        String[][] newField = underTest.placeShip(shipSize, field);

        //THEN
        assertEquals(newField[1][1], "x");
        assertEquals(newField[2][1], "x");
        assertEquals(newField[3][1], "x");
        assertEquals(newField[0][1], "b");
        assertEquals(newField[1][0], "b");
        assertEquals(newField[2][0], "b");
        assertEquals(newField[3][0], "b");
        assertEquals(newField[1][2], "b");
        assertEquals(newField[2][2], "b");
        assertEquals(newField[3][2], "b");

    }


}