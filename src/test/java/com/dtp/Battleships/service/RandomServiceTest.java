package com.dtp.Battleships.service;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
class RandomServiceTest {

    @InjectMocks
    RandomService underTest;


    @Test
    void generateRandomNumber() {

        // WHEN
        int result = underTest.generateRandomNumber(1);

        // THEN
        assertEquals(0, result);
    }
}