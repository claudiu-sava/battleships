package com.dtp.Battleships.service;

import com.dtp.Battleships.model.User;
import com.dtp.Battleships.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

@ExtendWith(SpringExtension.class)
class UserServiceTest {

    @InjectMocks
    UserService underTest;

    @Mock
    UserRepository userRepository;

    @Test
    void getUserByUsername() {

        //GIVEN
        User user = new User();
        user.setUsername("test");

        given(userRepository.findByUsername("test")).willReturn(Optional.of(user));


        //WHEN
        User result = underTest.getUserByUsername("test");

        //THEN
        assertEquals(user, result);
    }

    @Test
    void saveUser() {

        // GIVEN
        User user = new User();

        // WHEN
        underTest.saveUser(user);

        // THEN
        verify(userRepository).save(user);

    }
}