package com.dtp.Battleships.service;

import com.dtp.Battleships.model.MultiplayerGame;
import com.dtp.Battleships.repository.MultiplayerGameRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.HashMap;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
class MultiplayerGameServiceTest {

    @InjectMocks
    MultiplayerGameService underTest;

    @Mock
    MultiplayerGameRepository multiplayerGameRepository;
    @Mock
    GameService gameService;



    @Test
    void getMultiplayerGameById_isFound() {
        // GIVEN
        MultiplayerGame multiplayerGame = new MultiplayerGame();
        given(multiplayerGameRepository.findById(1)).willReturn(Optional.of(multiplayerGame));

        // WHEN
        MultiplayerGame result = underTest.getMultiplayerGameById(1);

        // THEN
        assertEquals(multiplayerGame, result);
    }

    @Test
    void getMultiplayerGameById_isNotFound() {
        // GIVEN
        given(multiplayerGameRepository.findById(1)).willReturn(Optional.empty());

        // WHEN
        MultiplayerGame result = underTest.getMultiplayerGameById(1);

        // THEN
        assertNull(result);
    }

    @Test
    void saveMultiplayerGame() {
        // GIVEN
        MultiplayerGame multiplayerGame = new MultiplayerGame();

        // WHEN
        underTest.saveMultiplayerGame(multiplayerGame);

        // THEN
        verify(multiplayerGameRepository).save(multiplayerGame);
    }

    @Test
    void shoot_notAllowed() {
        //GIVEN
        MultiplayerGame multiplayerGame = new MultiplayerGame();
        multiplayerGame.setTurn(1);

        //WHEN
        HashMap<String, Integer> result = underTest.shoot(0, 0, multiplayerGame, "p2");

        //THEN
        assertNull(result);

    }

    // TODO: Shoot hit


    // TODO: Shoot miss

    @Test
    void shoot_miss(){

    }

    @Test
    void calculateRatio() {

        //GIVEN
        String[][] field = {{"xO", "null"}, {"x", "null"}};

        //WHEN
        double result = underTest.calculateRatio(field);

        //THEN
        assertEquals(50, result);
    }
}