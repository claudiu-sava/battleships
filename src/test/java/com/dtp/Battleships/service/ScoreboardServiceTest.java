package com.dtp.Battleships.service;

import com.dtp.Battleships.model.Scoreboard;
import com.dtp.Battleships.repository.ScoreboardRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

@ExtendWith(SpringExtension.class)
class ScoreboardServiceTest {

    @InjectMocks
    ScoreboardService underTest;

    @Mock
    ScoreboardRepository scoreboardRepository;

    @Test
    void saveGameToScoreboard() {

        //GIVEN
        Scoreboard scoreboard = new Scoreboard();

        //WHEN
        underTest.saveGameToScoreboard(scoreboard);

        //THEN
        verify(scoreboardRepository).save(scoreboard);
    }

    @Test
    void getScoreboardForGame() {

        // GIVEN
        given(scoreboardRepository.findAllByGameIdOrderByScoreAsc(1)).willReturn(List.of(new Scoreboard()));

        // WHEN
        List<Scoreboard> result = underTest.getScoreboardForGame(1);

        // THEN
        assertEquals(1, result.size());

    }

    @Test
    void getScoreboardComplete() {

        //GIVEN
        given(scoreboardRepository.findAll()).willReturn(List.of(new Scoreboard()));

        //WHEN
        Iterable<Scoreboard> result = underTest.getScoreboardComplete();

        //THEN
        assertEquals(true, result.iterator().hasNext());

    }
}