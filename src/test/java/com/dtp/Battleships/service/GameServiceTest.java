package com.dtp.Battleships.service;

import com.dtp.Battleships.model.Game;
import com.dtp.Battleships.repository.GameRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.HashMap;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
class GameServiceTest {

    @InjectMocks
    GameService underTest;

    @Mock
    GameRepository gameRepository;
    @Mock
    RandomService randomService;

    @Test
    void saveGame() {
        //GIVEN
        Game game = new Game();

        //WHEN
        underTest.saveGame(game);

        //THEN
        verify(gameRepository).save(game);

    }

    @Test
    void cleanField() {

        //GIVEN
        String field = "nullO, null, x [[[[[[ xO bO, b ]]]]]]";

        //WHEN
        String result = underTest.cleanField(field);

        //THEN
        assertEquals("null, null, x [[ x b, b ]]", result);
    }

    @Test
    void cleanField_emptyField() {

        //GIVEN
        String field = "         ";

        //WHEN
        String result = underTest.cleanField(field);

        //THEN
        assertEquals("  ", result);

    }

    @Test
    void cleanGame() {

        //GIVEN
        Game game = spy(Game.class);
        String fieldEnemy = "nullO null x [[[[[[ xO bO b ]]]]]]";
        String fieldPlayer = "null [[[ nullO xO x b bO ]]]";

        game.setEnemyField(fieldEnemy);
        game.setPlayerField(fieldPlayer);

        //WHEN
        underTest.cleanGame(game);

        //THEN
        verify(game).setEnemyField("null null x [[ x b b ]]");
        verify(game).setPlayerField("null [[ null x x b b ]]");
    }

    @Test
    void resetPlayerField() {

        //GIVEN
        Game game = spy(Game.class);
        String fieldPlayer = "[['x', 'x'], ['null', 'x']]";
        game.setPlayerField(fieldPlayer);

        //WHEN
        underTest.resetPlayerField(game);

        //THEN
        verify(game).setPlayerField("[['null', 'null'], ['null', 'null']]");
    }


    @Test
    void getGameById_ifGameIsNotFound() {
        //GIVEN
        given(gameRepository.findById(234)).willReturn(Optional.empty());


        //WHEN
        Game result = underTest.getGameById(234);

        //THEN
        assertNull(result);


    }

    @Test
    void getGameById_ifGameIsFound() {
        //GIVEN
        given(gameRepository.findById(234)).willReturn(Optional.of(new Game()));


        //WHEN
        Game result = underTest.getGameById(234);

        //THEN
        assertNotNull(result);


    }

    @Test
    void fieldStringToArray() {

        //GIVEN
        String field = "[['x', 'null'], ['b', 'x']]";

        //WHEN
        String[][] result = underTest.fieldStringToArray(field);

        //THEN
        assertEquals(result instanceof String[][], true);

    }

    @Test
    void placeShip_ifCellIsNotOccupied() {
        //GIVEN
        String[][] field = {{"null", "null"}, {"b", "x"}};

        //WHEN
        int result = underTest.placeShip("x", 0, 0, field);

        //THEN
        assertEquals(1, result);
    }

    @Test
    void placeShip_ifCellIsOccupied() {
        //GIVEN
        String[][] field = {{"x", "null"}, {"b", "x"}};

        //WHEN
        int result = underTest.placeShip("x", 0, 0, field);

        //THEN
        assertEquals(2, result);
    }

    @Test
    void shoot_shootHitsShip() {

        // GIVEN
        String[][] field = {{"x", "null"}, {"b", "nullO"}};
        int positionY = 0;
        int positionX = 0;

        // WHEN
        HashMap<String, Integer> result = underTest.shoot(positionY, positionX, field);

        // THEN
        assertEquals(1, result.get("status"));

    }

    @Test
    void shoot_shootMissesShip() {

        // GIVEN
        String[][] field = {{"x", "null"}, {"b", "nullO"}};
        int positionY = 0;
        int positionX = 1;

        // WHEN
        HashMap<String, Integer> result = underTest.shoot(positionY, positionX, field);

        // THEN
        assertEquals(0, result.get("status"));

    }

    @Test
    void shoot_shootsOnTheSameCell() {

        // GIVEN
        String[][] field = {{"x", "null"}, {"b", "nullO"}};
        int positionY = 1;
        int positionX = 1;

        // WHEN
        HashMap<String, Integer> result = underTest.shoot(positionY, positionX, field);

        // THEN
        assertEquals(2, result.get("status"));

    }
    @Test
    void calculateRatio() {

        //GIVEN
        String[][] field = {{"x", "null"}, {"x", "null"}};

        //WHEN
        double result = underTest.calculateRatio(field);

        //THEN
        assertEquals(50, result);
    }

    //TODO
    @Test
    void shootBackNormal() {
    }

    @Test
    void getGameId() {

        //GIVEN
        Game game = new Game();
        game.setId(1);

        //WHEN
        int gameId = underTest.getGameId(game);

        //THEN
        assertEquals(1, gameId);


    }

    @Test
    void checkFieldSize_FieldIsBiggerThanAllowed() {

        // WHEN
        int result = underTest.checkFieldSize(99);

        // THEN
        assertEquals(15, result);

    }

    @Test
    void checkFieldSize_FieldIsSmallerThanAllowed() {

        // WHEN
        int result = underTest.checkFieldSize(1);

        // THEN
        assertEquals(12, result);

    }

    @Test
    void shootBackHacker_ShipsLeftToShot() {

        // GIVEN
        Game game = new Game();
        game.setPlayerField("[['xO', 'nullO'], ['xO', 'x'], ['b', 'null']]");
        game.setId(1);

        given(gameRepository.findById(1)).willReturn(Optional.of(game));

        // WHEN
        HashMap<String, Integer> answer = underTest.shootBackHacker(1);

        // THEN
        assertEquals(1, answer.get("status"));

    }

    @Test
    void shootBackHacker_NoShipsLeft() {

        // GIVEN
        Game game = new Game();
        game.setPlayerField("[['xO', 'nullO'], ['xO', 'xO'], ['b', 'null']]");
        game.setId(1);

        given(gameRepository.findById(1)).willReturn(Optional.of(game));

        // WHEN
        HashMap<String, Integer> answer = underTest.shootBackHacker(1);

        // THEN
        assertEquals(-1, answer.get("status"));

    }

}